<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>法律手续表管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/laws/atcLaws/">法律手续表列表</a></li>
		<shiro:hasPermission name="laws:atcLaws:edit"><li><a href="${ctx}/laws/atcLaws/form">法律手续表添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="atcLaws" action="${ctx}/laws/atcLaws/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			
			<li><label>姓名：</label>
				<form:input path="name" htmlEscape="false" maxlength="64" class="input-medium"/>
				 <span class="help-inline"><font >(戒毒人员)</font> </span>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
			    <th>人员编号</th>
				<th>戒毒人员姓名</th>
				<th>违反法律名称</th>
				<th>戒毒所名称</th>
				<th>戒毒所地址</th>
				
				<shiro:hasPermission name="laws:atcLaws:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="atcLaws">
			<tr>
			    <td>
					${atcLaws.NO}
				</td>
				<td><a href="${ctx}/laws/atcLaws/model?id=${atcLaws.id}">
					${atcLaws.name}
				</a></td>
				<td>
					${atcLaws.lawName}
				</td>
				<td>
					${atcLaws.drugRehabilitationCenter}
				</td>
				<td>
					${atcLaws.drugRehabilitationAddress}
				</td>
				
				<shiro:hasPermission name="laws:atcLaws:edit"><td>
    				<a href="${ctx}/laws/atcLaws/model?id=${atcLaws.id}">修改</a>
					<a href="${ctx}/laws/atcLaws/delete?id=${atcLaws.id}" onclick="return confirmx('确认要删除该法律手续表吗？', this.href)">删除</a>
					<a href="${ctx}/laws/atcLaws/printer?id=${atcLaws.id}">打印预览</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>