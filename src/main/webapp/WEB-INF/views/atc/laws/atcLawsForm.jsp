<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>法律手续表管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
				var id = $("#tableNameId").val();
					
					/*用户是否重复添加验证*/
					 var returnVal = true;
						$.ajax({
							url:"${ctx}/laws/atcLaws/findRole",
							type:"post",
							data:"tableName="+id,
							async:false,
							success:function(data){
								if(data=="true"){
									$("#nameId").text("用户已添加该资料")
									returnVal = false;
								}else{
									$("#nameId").text("*")
								}
							}
						});
						if(returnVal==false){
							return false;
						}
				
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
	<style>
	 table{
	 width:1200px;
	text-align:left;
	 }
	 td{
	 width:250px;
	 height:50px;
	 }
	 #write{
	 width:165px;
	 }
	 .write{
	 width:160px;
	 }
	 .area{
	 resize:none;
  margin : 10px 10px 10px 10px;
	 width:970px;}
	 #title{
	 font-size:20px;font-weight:bold;height:60px
	 }
	
	</style>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/laws/atcLaws/">法律手续表列表</a></li>
		<li class="active"><a href="${ctx}/laws/atcLaws/form?id=${atcLaws.id}">法律手续表<shiro:hasPermission name="laws:atcLaws:edit">${not empty atcLaws.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="laws:atcLaws:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="atcLaws" action="${ctx}/laws/atcLaws/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
<table>	
   <tr>
       <td>		
		
			<label class="control-label">公文头公：</label>
			</td>
	    <td>		
			
				<form:input path="gong" htmlEscape="false" maxlength="10" class="input-xlarge required" id="write"/>
		 <span class="help-inline"><font color="red">*</font> </span>
				    <span class="help-inline"><font >(如（鲁）)</font> </span>
		</td>
		<td>
			<label class="control-label">姓名：</label>
		</td>
		<td>	
			 <sys:treeselect id="tableName" name="tableName" value="${atcLaws.tableName}" labelName="NO" labelValue="${atcLaws.name}"
					title="用户" url="/atcbase/atcBase/treeData?type=3" cssClass="input-small required" allowClear="true" notAllowSelectParent="true"/>
					<span class="help-inline"><font color="red" id="nameId">*</font> </span>
		<span class="help-inline"><font>（戒毒人）</font> </span>
		</td>
		<td>
		
			<label class="control-label">公文头第：</label>
		</td>
		<td>	
			
				<form:input path="di" htmlEscape="false" maxlength="20" class="input-xlarge required write" />
			<span class="help-inline"><font color="red">*</font> </span>
		</td>
		</tr>	
			
</table>
<table>			
    <tr>
		<td>
		<div class="control-group">
			<label class="control-label">违法事件说明:</label>
			<div class="controls">
				<form:textarea path="unlawfulAct" htmlEscape="false" rows="4" maxlength="200" class="input-xxlarge  area required"/>
				<span class="help-inline"><font color="red">*</font> </span>
		</div></div>
		
		</td>
   </tr>
			
</table>
<table>			
	<tr><td>	
	
			<label class="control-label">违反法律名称：</label>
		</td>
		<td>	
		
				<form:input path="lawName" htmlEscape="false" maxlength="30" class="input-xlarge required write" />
		<span class="help-inline"><font color="red">*</font> </span>
		</td>
		<td>
	
			<label class="control-label">法律条目：</label>
		</td>
		<td>	
			
				<form:input path="lawStrip" htmlEscape="false" maxlength="20" class="input-xlarge required write" />
		<span class="help-inline"><font color="red">*</font> </span>
		</td>
		<td>
		
			<label class="control-label">法律款数：</label>
		</td>
		<td>	
				<form:input path="lawSincere" htmlEscape="false" maxlength="20" class="input-xlarge " id="write"/>
			
		</td>
   </tr>
   <tr>
		<td>	
		
	
			<label class="control-label">法律项数：</label>
		</td>
		<td>	
		
				<form:input path="lawTerms" htmlEscape="false" maxlength="20" class="input-xlarge " id="write"/>
			
		</td>
	</tr>	
</table>
<table>			
		<tr><td>
			  <div class="control-group">
			<label class="control-label">备注：</label>
			<div class="controls">
				<form:textarea path="remark" htmlEscape="false" rows="4" maxlength="200" class=" area"/>
			</div>
		</div></td>
     </tr>
</table>
<table>			
	<tr><td>	</div>
		
			<label class="control-label">复议机关：</label>
		</td>
		<td>	
		
				<form:input path="reconsiderDepartment" htmlEscape="false" maxlength="40" class="input-xlarge required write" />
		<span class="help-inline"><font color="red">*</font> </span>
		</td>
		<td>
		
			<label class="control-label">戒毒所名称：</label>
		</td>
		<td>	
		
				<form:input path="drugRehabilitationCenter" htmlEscape="false" maxlength="40" class="input-xlarge required write" />
		<span class="help-inline"><font color="red">*</font> </span>
		</td>
		<td>
	
			<label class="control-label">戒毒所地址：</label>
		</td>
		<td>	
		
				<form:input path="drugRehabilitationAddress" htmlEscape="false" maxlength="40" class="input-xlarge required write" />
			<span class="help-inline"><font color="red">*</font> </span>
		</td>
	<tr>		
			
</table>			
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="laws:atcLaws:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			
		</div>
	</form:form>
</body>
</html>