<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta name="decorator" content="default"/>
	<link type="text/css" rel="stylesheet" href="${ctxStatic}/css/atc/atcLawPrintView.css" />
	<script type="text/javascript">
			function printdiv(printpage) 
			{ 
			var headstr = "<html><head><title></title></head><body>"; 
			var footstr = "</body>"; 
			var newstr = document.all.item(printpage).innerHTML; 
			var oldstr = document.body.innerHTML; 
			document.body.innerHTML = headstr+newstr+footstr; 
			window.print(); 
			document.body.innerHTML = oldstr; 
			return false; 
	} 
	
	</script>
	
	<style type="text/css">
	.btn-primary{
		margin-left: 800px;
		margin-top: 50px;
	}
	</style>
</head>
<body>
	  

 
	<div id="div_print"> 
 
	
	<div><sys:message content="${message}"/></div>
		<div class="div_02">
			<table>
				<caption>
					<div class="div_span_01"><span>强制戒毒/延长强制戒毒决定书</span></div>
					<div class="div_span_02"><span> 公（${atcLaws.gong}）强戒决字[${atcLaws.NO}]第(${atcLaws.di})号</span></div>
				</caption>
				<tr class="tr_">
					<td class="td07" colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;被强制戒毒人是${atcLaws.name}，性别为${atcLaws.sex}。						
					现今他所携带的的身份证件种类为${atcLaws.identityType}，该证件号码是${atcLaws.idcardNumber}。出生于<fmt:formatDate value="${atcLaws.birthdate }" pattern="yyyy-MM-dd"/>。现居住在${atcLaws.nowPlace}。戒毒前在${atcLaws.workUnit}单位工作。如今查明:${atcLaws.unlawfulAct}</td>
				</tr>
				<tr class="tr_">
					<td class="td07" colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;根据《${atcLaws.lawName}》第（${atcLaws.lawStrip}）项规定，
						我局决定对其强制戒毒/延长强制戒毒${atcLaws.remark}自<fmt:formatDate value="${atcLaws.startTime }" pattern="yyyy-MM-dd"/>到<fmt:formatDate value="${atcLaws.endTime }" pattern="yyyy-MM-dd"/>。对不服本决定的可以在收到本决议书之日起，六十日内向${atcLaws.reconsiderDepartment}申请行政复议。
					</td>
				</tr>
				<tr class="tr_">
					<td class="td01">强制戒毒所名称</td>
					<td class="td06" colspan="5">${atcLaws.drugRehabilitationCenter}</td>
				</tr>
				<tr class="tr_">
					<td class="td05">地址</td>
					<td class="td06" colspan="5">${atcLaws.drugRehabilitationAddress}</td>
				</tr>
				<tr class="tr_">
					<td class="td03" colspan="6">(公安机关印章)</td>
				</tr>
				<tr class="tr_">
					<td class="td03" colspan="6">___年___月___日</td>
				</tr>
				<tr class="tr_">
					<td class="td03" colspan="6">被强制戒毒人（签名）：___________</td>
				</tr>
				<tr class="tr_">
					<td class="td03" colspan="6">___年___月___日</td>
				</tr>
			</table>
		</div>
	
	
 
</div> 
	<input name="b_print" type="button" class="btn btn-primary" onClick="printdiv('div_print');" value=" 打印 " > 
</body>
</html>