<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<meta name="decorator" content="default"/>
	<link type="text/css" rel="stylesheet" href="${ctxStatic}/css/atc/atcHealthPrintView.css" />
	<script type="text/javascript">
			function printdiv(printpage) 
			{ 
			var headstr = "<html><head><title></title></head><body>"; 
			var footstr = "</body>"; 
			var newstr = document.all.item(printpage).innerHTML; 
			var oldstr = document.body.innerHTML; 
			document.body.innerHTML = headstr+newstr+footstr; 
			window.print(); 
			document.body.innerHTML = oldstr; 
			return false; 
	} 
	
	</script>
	<style type="text/css">
	.btn-primary{
		margin-left: 800px;
		margin-top: 50px;
	}
	</style>
</head>
<body>
	  

 
	<div id="div_print"> 
 
	
	<div><sys:message content="${message}"/></div>
		<div class="div_table">
			<table>
				<caption>戒毒人员健康表</caption>
				<tr class="tr_">
					<td rowspan="2" class="td01">病历情况</td>
					<td class="td01">病室</td>
					<td class="td02" >${atcHealth.room}</td>
					<td class="td01">病房号</td>
					<td class="td02" >${atcHealth.room}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">住院号</td>
					<td class="td02" >${atcHealth.nu}</td>
					<td class="td01">床号</td>
					<td class="td02" >${atcHealth.bed}</td>
				</tr>
				<tr class="tr_">
					<td rowspan="4" class="td01">特殊病情况</td>
					<td class="td01">该员确定为</td>
					<td colspan="3" class="td02" >${atcHealth.sureState}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">到院检查情况</td><%--?--%>
					<td colspan="3" class="td02" >${atcHealth.sure}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">特殊病情</td>
					<td class="td02">${atcHealth.specialCondition}</td>
					<td class="td01">自杀/自伤情况</td>
					<td class="td02">${atcHealth.autotomy}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">其它情况</td>
					<td colspan="3" class="td02">${atcHealth.other}</td>
				</tr>
				<tr class="tr_">
					<td rowspan="4" class="td01">长期医嘱单</td>
					<td class="td01">开始时间</td>
					<td class="td02"><fmt:formatDate value="${atcHealth.start}" pattern="yyyy-MM-dd"/></td>
					<td class="td01">停止时间</td>
					<td class="td02"><fmt:formatDate value="${atcHealth.end}" pattern="yyyy-MM-dd"/></td>
				</tr>
				<tr class="tr_">
					<td class="td01">开始时医嘱</td>
					<td class="td02">${atcHealth.startAdvice}</td>
					<td class="td01">结束时医嘱</td>
					<td class="td02">${atcHealth.endAdvice}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">开始时医师</td>
					<td class="td02">${atcHealth.startDoctor}</td>
					<td class="td01">结束时医师</td>
					<td class="td02">${atcHealth.endDoctor}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">开始时护士</td>
					<td class="td02">${atcHealth.startNurse}</td>
					<td class="td01">结束时护士</td>
					<td class="td02">${atcHealth.endNurse}</td>
				</tr>
				<tr class="tr_">
					<td rowspan="5" class="td01">生物表</td>
					<td class="td01">日期</td><%--?--%>
					<td class="td02"></td>
					<td class="td01">脉搏</td>
					<td class="td02">${atcHealth.pulse}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">体温</td>
					<td class="td02">${atcHealth.thermometer}</td>
					<td class="td01">呼吸</td>
					<td class="td02">${atcHealth.breathe}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">大便次数</td>
					<td class="td02">${atcHealth.shitTimes}</td>
					<td class="td01">血压</td>
					<td class="td02">${atcHealth.bloodPressure}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">总入量</td>
					<td class="td02">${atcHealth.totalInput}</td>
					<td class="td01">总出量</td>
					<td class="td02">${atcHealth.totalOutput}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">尿量</td>
					<td class="td02">${atcHealth.urineVolume}</td>
					<td class="td01">体重</td>
					<td class="td02">${atcHealth.weight}</td>
				</tr>
			</table>
		</div>
	
	
 
</div> 
	<input name="b_print" type="button" class="btn btn-primary" onClick="printdiv('div_print');" value=" 打印 " > 
</body>
</html>