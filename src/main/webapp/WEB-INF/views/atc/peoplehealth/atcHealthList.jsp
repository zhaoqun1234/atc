<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>保存健康检查信息成功管理</title>
	<meta name="decorator" content="default"/>
	<link type="text/css" rel="stylesheet" href="${ctxStatic}/css/atc/atcHealthList.css" />
	<%-- <script type="text/javascript" src="${ctxStatic}/js/atc/" ></script> --%>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/peoplehealth/atcHealth/">人员健康检查信息列表</a></li>
	<shiro:hasPermission name="peoplehealth:atcHealth:edit">
		<li><a href="${ctx}/peoplehealth/atcHealth/form">人员健康检查信息添加</a></li>
	</shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="atcHealth" action="${ctx}/peoplehealth/atcHealth/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>姓名:</label>
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>病房号:</label>
				<form:input path="room" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>人员编号</th>
				<th>姓名</th>
				<th>病房号</th>
				<th>病床号</th>
				<th>住院号</th>
				<th>确诊情况</th>
				<th>自残情况</th>
				<shiro:hasPermission name="peoplehealth:atcHealth:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="atcHealth">
			<tr>
				<td>${atcHealth.peopleNo}</td>
				<td><a href="${ctx}/peoplehealth/atcHealth/view?id=${atcHealth.id}">${atcHealth.name}</a></td>
				<td>${atcHealth.room}</td>
				<td>${atcHealth.bed}</td>
				<td>${atcHealth.nu}</td>
				<td>${atcHealth.sureState}</td>
				<td>${atcHealth.autotomy}</td>
   				<%-- <a href="${ctx}/peoplehealth/atcHealth/atcLawPrintView">预览</a> --%>
				<shiro:hasPermission name="peoplehealth:atcHealth:edit"><td>
	   				<%-- <a href="${ctx}/peoplehealth/atcHealth/LawView">预览Law</a> --%>
    				<a href="${ctx}/peoplehealth/atcHealth/form?id=${atcHealth.id}">修改</a>
					<a href="${ctx}/peoplehealth/atcHealth/delete?id=${atcHealth.id}" onclick="return confirmx('确认要删除该保存健康检查信息成功吗？', this.href)">删除</a>
					<a href="${ctx}/peoplehealth/atcHealth/printer?id=${atcHealth.id}">打印预览</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>