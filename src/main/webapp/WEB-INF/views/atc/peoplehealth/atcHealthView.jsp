<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>人员健康表-详情展示</title>
	<meta name="decorator" content="default"/>
	<link type="text/css" rel="stylesheet" href="${ctxStatic}/css/atc/atcHealthView.css" />
	<script type="text/javascript">
		var ctx ="${ctx}";
	</script>
</head>
<body>
	<div class="div_01">
		<ul class="nav nav-tabs">
			<li><a href="${ctx}/peoplehealth/atcHealth/">人员健康检查信息列表</a></li>
			<li class="active">
				<a href="${ctx}/peoplehealth/atcHealth/view?id=${atcHealth.id}">人员健康表检查信息 详情展示</a>
			</li>
		</ul><br/>
		<form:form id="inputForm" modelAttribute="atcHealth" action="${ctx}/peoplehealth/atcHealth/save" method="post" class="form-horizontal">
			<form:hidden path="id"/>
			<sys:message content="${message}"/>		
			<div class="control-group">
				<label class="control-label">姓名:</label>
				<div class="controls">
					<sys:treeselect id="fkId" name="fkId" value="${atcHealth.fkId}" labelName="name" labelValue="${atcHealth.name}"
					title="用户" url="/atcbase/atcBase/treeData?type=3" disabled="disabled" cssClass="" allowClear="true" notAllowSelectParent="true"/>
				<span id="msgfkId" class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group control-group01">
				<label class="control-label">病房号:</label>
				<div class="controls">
					<form:input path="room" htmlEscape="false" maxlength="255" disabled="true" class="input-xlarge input-xlarge01 required digits"/>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group control-group01">
				<label class="control-label">病床号:</label>
				<div class="controls">
					<form:input path="bed"  htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge01 required digits"/>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">住院号:</label>
				<div class="controls">
					<form:input path="nu" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge03 required digits"/>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group control-group01">
				<label class="control-label">确诊:</label>
				<div class="controls">
					<form:input path="sure" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge01 required"/>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group control-group01">
				<label class="control-label">确诊情况:</label>
				<div class="controls">
					<form:input path="sureState" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge01 required"/>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">自残情况:</label>
				<div class="controls">
					<form:input path="autotomy" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge03"/>
				</div>
			</div>
			<div class="control-group control-group03">
				<label class="control-label">开始时间:</label>
				<div class="controls">
					<input id="start" name="start" type="text" readonly="readonly" maxlength="20" disabled="true"  class="input-medium Wdate required"
						value="<fmt:formatDate value="${atcHealth.start}" pattern="yyyy-MM-dd"/>"
						onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
					<span id="msgstart" class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group control-group01">
				<label class="control-label">开始时医生:</label>
				<div class="controls">
					<form:input path="startDoctor" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge01 required"/>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">开始时护士:</label>
				<div class="controls">
					<form:input path="startNurse" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge03 required"/>
					<span id="msgstartNurse" class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group control-group03">
				<label class="control-label">结束时医生:</label>
				<div class="controls">
					<form:input path="endDoctor" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge01"/>
				</div>
			</div>
			<div class="control-group control-group01">
				<label class="control-label">结束时护士:</label>
				<div class="controls">
					<form:input path="endNurse" htmlEscape="false" maxlength="255" disabled="true" class="input-xlarge input-xlarge01"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">结束时间:</label>
				<div class="controls">
					<input id="end" name="end" type="text" readonly="readonly" maxlength="20" disabled="true" class="input-medium Wdate rightThree required"
						value="<fmt:formatDate value="${atcHealth.end}" pattern="yyyy-MM-dd"/>"
						onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
					<span id="msgend" class="help-inline">*</span>						
				</div>
			</div>
			<div class="control-group control-group01">
				<label class="control-label">脉搏:</label>
				<div class="controls">
					<form:input path="pulse" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge02 required number"/>
					<span>次/分钟</span>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group control-group01">
				<label class="control-label">体温:</label>
				<div class="controls">
					<form:input path="thermometer" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge02 required number"/>
					<span>℃</span>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">呼吸:</label>
				<div class="controls">
					<form:input path="breathe" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge04 required digits"/>
					<span>次/分钟</span>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group control-group01">
				<label class="control-label">大便次数:</label>
				<div class="controls">
					<form:input path="shitTimes" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge02 required digits"/>
					<span>次/天</span>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group control-group01">
				<label class="control-label">血压:</label>
				<div class="controls">
					<form:input path="bloodPressure" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge02 required number"/>
					<span>mmHg</span>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">尿量:</label>
				<div class="controls">
					<form:input path="urineVolume" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge04 required number"/>
					<span>ml/天</span>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group  control-group01">
				<label class="control-label">体重:</label>
				<div class="controls">
					<form:input path="weight" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge02 required number"/>
					<span>kg</span>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group  control-group01">
				<label class="control-label">总入量:</label>
				<div class="controls">
					<form:input path="totalInput" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge01 required number"/>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">总出量:</label>
				<div class="controls">
					<form:input path="totalOutput" htmlEscape="false" maxlength="255" disabled="true"  class="input-xlarge input-xlarge03 required number"/>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group control-group02">
				<label class="control-label">病史:</label>
				<div class="controls">
					<form:textarea path="history" htmlEscape="false" rows="4" maxlength="255" disabled="true"  class="input-xxlarge input-xxlarge01"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">特殊病情:</label>
				<div class="controls">
					<form:textarea path="specialCondition" htmlEscape="false" rows="4" maxlength="255" disabled="true"  class="input-xxlarge rightThree"/>
				</div>
			</div>
			<div class="control-group control-group02">
				<label class="control-label">开始时医嘱:</label>
				<div class="controls">
					<form:textarea path="startAdvice" htmlEscape="false" rows="4" maxlength="255" disabled="true"  class="input-xxlarge  required"/>
					<span class="help-inline">*</span>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">结束时医嘱:</label>
				<div class="controls">
					<form:textarea path="endAdvice" htmlEscape="false" rows="4" maxlength="255" disabled="true"  class="input-xxlarge rightThree"/>
				</div>
			</div>
			<div class="control-group control-group04">
				<label class="control-label">其他情况:</label>
				<div class="controls">
					<form:textarea path="other" htmlEscape="false" rows="4" maxlength="255" disabled="true" class="input-xxlarge input-xxlarge02"/>
				</div>
			</div>
		</form:form>
	</div>
</body>
</html>