<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<script src="${ctxStatic}/jquery/jquery-1.8.3.min.js"></script>
<script src="${ctxStatic}/jquery/highcharts.js"></script>
<script language="JavaScript">
$(document).ready(function() {  
  /* 此处建立二维数组 */
   var arr = new Array();
	   <c:forEach items="${key }" var="map">
		 var array=new Array();
	    array.push( "${map['culture'] }");
	    array.push(${map['sun']});
	    arr.push(array);
	   </c:forEach>
	var chart = {
       plotBackgroundColor: null,
       plotBorderWidth: null,
       plotShadow: false
   };
   var title = {
      text: '威海市戒毒所不同学历吸毒比例',
      style: {
          color: 'red',
          'fontSize' : '24px'
       }
   };      
   var tooltip = {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
   };
   var plotOptions = {
      pie: {
         allowPointSelect: true,
         cursor: 'pointer',
         dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
            style: {
               color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            }
         }
      }
   };
   var series= [{
      type: 'pie',
      name: 'Browser share',
      data: arr
   }];     
      
   var json = {};   
   json.chart = chart; 
   json.title = title;     
   json.tooltip = tooltip;  
   json.series = series;
   json.plotOptions = plotOptions;
   $('#container').highcharts(json);  
});
</script>
</head>
<body>
<div id="container" style="width: 650px; height: 500px; margin: 0 auto ; margin-top: 100px"></div>

</body>
</html>

