<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>保存基本信息成功管理</title>
	<meta name="decorator" content="default"/>
	 <script type="text/javascript">
	 $(document).ready(function() {
			$("#telephone").blur(
			function(){
			/* 这里是校验电话号码 */
			 var  tele=$("#telephone").val();
				 		 if(/^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/.test(tele)){
				 			$("#teleInfo").text("*");
				 		}else{
							 $("#teleInfo").text("电话的格式不正确");
							 
						 }
			}
			);
			$("#idcardNumber").focus(
			function(){
			 var type=$("#identityType").val();
			 if(type==""){
			 $("#type1").text("请先填写证件类型");
			 }else{
			  $("#type1").text("*");
			  $("#idcardNumber").blur(
			  function(){
			  
			  var  te=$("#identityType").val();
			  var  te1=$("#idcardNumber").val();
			  if(te=="身份证"){
			   if(/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(te1)){
				 			$("#type1").text("*");
				 		}else{
							 $("#type1").text("身份证格式不正确");
							 
						 }
			  }
			  if(te=="护照"){
			     if(/^1[45][0-9]{7}|G[0-9]{8}|P[0-9]{7}|S[0-9]{7,8}|D[0-9]+$/.test(te1)){
			     $("#type1").text("*");
			     }else{
			     $("#type1").text("护照的格式不正确");
			     }
			  }
				 		
			  
			  
			  
			  
			  }
			  )
			  
			  
			  
			 }
			}
			)
			
			$("#inputForm").validate({
				 submitHandler: function(form){
				
				  
					loading('正在提交，请稍等...');
					form.submit();
				},  
			 	 errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}  
			}); 
		});  
	 
	 
	 
	 
	 
		
		
		
	</script>
	
	<style>
	 table{
	 width:1600px;
	 
	 }
	 td{
	 width:1300px;
	 height:60px;

	 }
	 .wr{
	 width:150px
	 }
	 .write{
	 width:160px
	 }
	 .area{
	 resize:none;
	 width:1000px}
	 #title{
	 font-size:20px;font-weight:bold;height:60px
	 }
	 #relationship input{
	 width:150px;
	 }
	 .Border td{
	 border:1px solid #cccccc;
	 }
	</style>
</head>
<body>
	<ul class="nav nav-tabs">
		
		<li class="active"><a href="${ctx}/atcbase/atcBase/form?id=${atcBase.id}">人员基本信息录入<shiro:hasPermission name="atcbase:atcBase:edit">${not empty atcBase.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="atcbase:atcBase:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="atcBase" action="${ctx}/atcbase/atcBase/save" method="post" class="form-horizontal">
		 <form:hidden path="id"/> 
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label" id="title">人员编号：</label>
			<div class="controls">
				<form:input path="no" htmlEscape="false" maxlength="255" class="input-xlarge wr"  readonly="true"/>
			</div>
		</div>
		<table>
		<tr ><td id="title">人员基本信息录入</td></tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">姓名：</label>
			<div class="controls">
				<form:input path="name" htmlEscape="false" maxlength="10" class="input-xlarge wr required"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">别名：</label>
			<div class="controls">
				<form:input path="alias" htmlEscape="false" maxlength="10" class="input-xlarge wr" />
				
			</div>
		</div>
		</td>
		<td>
				<div class="control-group">
			<label class="control-label">性别：</label>
			<div class="controls">
				<form:select path="sex" style="width:170px" class="required">
				<form:option value="" label="请选择"/>
					<form:option value="男" label="男"/>
					<form:option value="女" label="女"/>
					<form:option value="其他" label="其他"/>
					<form:options items="${fns:getDictList('')}" itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			
			</div>
		</div>
		
		
		
		</td>
		
		<td >
	        <!--  <div class="control-group"> -->
			
		   照片(<span style="color:red">请上传1寸的照片</span>):<div style="width:150px;height:150px">
				<form:hidden id="imagePeople" path="imagePeople" htmlEscape="false" maxlength="1000" class="input-xlarge" />
				<sys:ckfinder input="imagePeople" type="images" uploadPath="/atcbase/atcBase" selectMultiple="true" />
			</div>
		<!-- </div> -->
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">婚姻状态：</label>
			<div class="controls">
				<form:select path="marital" style="width:170px" class="required">
				<form:option value="" label="请选择"/>
					<form:option value="已婚" label="已婚"/>
					<form:option value="未婚" label="未婚"/>
					<form:option value="离异" label="离异"/>
					<form:option value="其他" label="其他"/>
					<form:options items="${fns:getDictList('marital')}" itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			
			</div>
		</div>
		</td>
		<td>
		
			<div class="control-group">
			<label class="control-label">出生日期：</label>
			<div class="controls">
				<input name="birthdate" type="text" maxlength="20" class="input-medium Wdate wr required"
					value="<fmt:formatDate value="${atcBase.birthdate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
			
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">民族：</label>
			<div class="controls">
				<form:input path="nation" htmlEscape="false" maxlength="8" class="input-xlarge wr required" placeholder="(汉)"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">文化程度：</label>
			<div class="controls">
				<form:select path="culture" class="input-xlarge write required">
					<form:option value="" label="请选择"/>
					<form:option value="文盲" label="文盲"/>
					<form:option value="小学" label="小学"/>
					<form:option value="初中" label="初中"/>
					<form:option value="高中" label="高中"/>
					<form:option value="大专" label="大专"/>
					<form:option value="本科及以上" label="本科及以上"/>
					<form:options items="${fns:getDictList('culture')}" itemLabel="label" itemValue="value" htmlEscape="false" class="wr"/>
				</form:select>
				
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">特殊体征：</label>
			<div class="controls">
				<form:input path="specialSigns" htmlEscape="false" maxlength="8" class="input-xlarge wr" placeholder="例:有纹身"/>
			</div>
		</div>
		
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">国籍：</label>
			<div class="controls">
				<form:input path="country" htmlEscape="false" maxlength="10" class="input-xlarge wr required" placeholder="(中国)"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">籍贯：</label>
			<div class="controls">
				<form:input path="nativePlace" htmlEscape="false" maxlength="8" class="input-xlarge wr" placeholder="(鲁)"/>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">职业：</label>
			<div class="controls">
				<form:input path="job" htmlEscape="false" maxlength="8" class="input-xlarge wr required"/>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		
		
		
	<div class="control-group">
			<label class="control-label">户口所在地：</label>
			<div class="controls" style="width:600px">
				<form:input path="address" htmlEscape="false" maxlength="18" class="input-xlarge required" style="width:550px"/>
			
			</div>
		</div>
		</td>
		<td></td>
		<td><div class="control-group">
			<label class="control-label">现居地：</label>
			<div class="controls" style="width:600px">
				<form:input path="nowPlace" htmlEscape="false" maxlength="18" class="input-xlarge required" style="width:550px"/>
				
			</div>
		</div></td>
		<td>
		
		</td>
	
	
		
		
		</tr>
		<tr>
			<td>
		<div class="control-group" style="width:1000px">
		<label class="control-label">证件类型：</label>
		&nbsp;&nbsp;&nbsp;&nbsp;
			<form:select path="identityType" class="input-xlarge required" style="width:150px">
					<form:option value="" label="请选择"/>
					<form:option value="身份证" label="身份证"/>
					<form:option value="护照" label="护照"/>
					<form:option value="其他" label="其他"/>
					<form:options items="${fns:getDictList('identityType')}" itemLabel="label" itemValue="value" htmlEscape="false" class="wr"/>
				</form:select>
				<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><form:input path="idcardNumber" htmlEscape="false" maxlength="20" class="input-xlarge number required" style="width:380px;" placeholder="输入证件号码" /><span id="type1" style="color:red"></span>
				
			
		</div>
		
		</td>
		<td></td>
		<td>
		<div class="control-group">
			<label class="control-label">工作单位：</label>
			<div class="controls" style="width:600px">
				<form:input path="workUnit" htmlEscape="false" maxlength="18" class="input-xlarge required" style="width:550px"/>
			</div>
		</div></td>
			<td>
		
		</td>
		</tr>
		</table>
		<table>
		<tr><td id="title">吸毒信息</td></tr>
		 <tr>
		 <td>
		<div class="control-group">
			<label class="control-label">初次吸毒时间：</label>
			<div class="controls">
				<input name="fristDate" type="text"  maxlength="20" class="input-medium Wdate wr required"
					value="<fmt:formatDate value="${atcBase.fristDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">以往吸毒次数：</label>
			<div class="controls">
				<form:input path="times" htmlEscape="false" maxlength="3" class="input-xlarge  number wr required" placeholder="(次)"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">以往戒毒次数：</label>
			<div class="controls">
				<form:input path="timesGiveupDrug" htmlEscape="false" maxlength="3" class="input-xlarge number wr" placeholder="(次)"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">吸食方式：</label>
			<div class="controls">
				<form:input path="ways" htmlEscape="false" maxlength="7" class="input-xlarge wr required" placeholder="例:注射"/>
				
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">强制戒毒起始日期：</label>
			<div class="controls">
				<input name="starttime" type="text"  maxlength="20" class="input-medium Wdate wr required"
					value="<fmt:formatDate value="${atcBase.starttime}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">强制戒毒终止日期：</label>
			<div class="controls">
				<input name="endtime" type="text"  maxlength="20" class="input-medium Wdate wr required"
					value="<fmt:formatDate value="${atcBase.endtime}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">吸毒种类：</label>
			<div class="controls">
				<form:input path="drugVarieties" htmlEscape="false" maxlength="8" class="input-xlarge wr required"/>
				
			</div>
		</div>
		</td>
		<td>
		
			<div class="control-group">
			<label class="control-label">强制戒毒公安单位：</label>
			<div class="controls">
				<form:select path="police" class="input-xlarge write required">
				<form:option value="" label="请选择"/>
					<form:option value="威海公安局侦查支队" label="威海刑事侦查支队"/>
					<form:option value="威海市看守所" label="威海市看守所"/>
					<form:option value="威海市公安局分局" label="威海市公安局分局"/>
					<form:option value="威海市公安局" label="威海市公安局"/>
					<form:option value="其他" label="其他"/>
					<form:options items="${fns:getDictList('police')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			
			</div>
		</div>
		
		
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">公安机关联系电话：</label>
			<div class="controls">
				<form:input  path="telephone" htmlEscape="false" maxlength="12" class="input-xlarge wr required" placeholder="0551-6645266"/>
			     <span id="teleInfo" style="color:red;font-weight:bold"></span> 
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">入所日期：</label>
			<div class="controls">
				<input name="inDate" type="text"  maxlength="20" class="input-medium Wdate wr required"
					value="<fmt:formatDate value="${atcBase.inDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">其他并行处罚：</label>
			<div class="controls">
				<form:input path="punishMoney" htmlEscape="false" maxlength="10" class="input-xlarge wr required"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">办案单位：</label>
			<div class="controls">
				<form:input path="handleUntis" htmlEscape="false" maxlength="10" class="input-xlarge wr required"/>
			
			</div>
		</div>
	
	
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">病床号：</label>
			<div class="controls">
				<form:input path="bedNumber" htmlEscape="false" maxlength="8" class="input-xlarge wr required"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">医护人员：</label>
			<div class="controls">
				<form:input path="medicalWork" htmlEscape="false" maxlength="8" class="input-xlarge wr required"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">入所分类：</label>
			<div class="controls">
				<form:select path="category" class="input-xlarge write required">
				<form:option value="" label="请选择"/>
					<form:option value="自愿" label="自愿"/>
					<form:option value="强制" label="强制"/>
					<form:options items="${fns:getDictList('category')}" itemLabel="label" itemValue="value" htmlEscape="false" class="wr"/>
				</form:select>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">人员分类：</label>
			<div class="controls">
				<form:select path="personClassification" class="input-xlarge write required">
				<form:option value="" label="请选择"/>
					<form:option value="转拘人员" label="转拘人员"/>
					<form:option value="拟牢人员" label="拟牢人员"/>
					<form:options items="${fns:getDictList('')}" itemLabel="label" itemValue="value" htmlEscape="false" class="wr"/>
				</form:select>
			
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">管教民警：</label>
			<div class="controls">
				<form:input path="policeDisciline" htmlEscape="false" maxlength="8" class="input-xlarge wr required"/>
			
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">协管民警：</label>
			<div class="controls">
				<form:input path="assistPolice" htmlEscape="false" maxlength="8" class="input-xlarge wr required"/>
			
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">戒毒费：</label>
			<div class="controls">
				<form:input path="drugFee" htmlEscape="false" maxlength="7" class="input-xlarge  number wr required"  placeholder="(元)"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">零用钱：</label>
			<div class="controls">
				<form:input path="pocketMoney" htmlEscape="false" maxlength="7" class="input-xlarge number wr" placeholder="(元)"/>
			
			</div>
		</div>
		</td>
		</tr>
		</table>
		<div class="control-group">
			<label class="control-label">何时何地经过机关处理：</label>
			<div class="controls">
				<form:textarea path="officeDealWith" htmlEscape="false" rows="4" maxlength="200" class="area required" placeholder="请输入1-200个字"/>
				
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">戒毒人员个人简历：</label>
			<div class="controls">
				<form:textarea path="drugPeopleIntroduce" htmlEscape="false" rows="4" maxlength="200" class="input-xxlarge area required"  placeholder="请输入1-200个字"/>
				
			</div>
		</div> 
		<p id="title">家庭人员及关系:<p>
		
		
		<div class="control-group">
			<label class="control-label"></label>
			<div class="controls">
				<table id="relationship" style="width:800px;border:1px solid #cccccc" class="Border">
				<tr style="height:10px">
				<td>姓名</td>
				<td>与该人员关系</td>
				<td>工作单位</td>
				<td>住所</td>
				<td>联系电话</td>
				
				</tr>
				<tr style="height:10px">
				<td style="height:10px"><input type="text" name="nameRelationship" style="width:160px;border:1px solid white"/></td>
				<td style="height:10px"><input  type="text" name="relationship_people"style="width:160px;border:1px solid white"/></td>
				<td style="height:10px"><input type="text" name="work_unit" style="width:220px;border:1px solid white"/></td>
				<td style="height:10px"><input  type="text" name="stay_place" style="width:220px;border:1px solid white"/></td>
				<td style="height:10px"><input type="text" name="telephone_number" style="width:160px;border:1px solid white"/></td>
				
				</tr>
				<tr style="height:10px">
				<td style="height:10px"><input type="text" name="nameRelationship"style="width:160px;border:1px solid white"/></td>
				<td style="height:10px"><input  type="text" name="relationship_people"style="width:160px;border:1px solid white"/></td>
				<td style="height:10px"><input type="text" name="work_unit" style="width:220px;border:1px solid white"/></td>
				<td style="height:10px"><input  type="text" name="stay_place" style="width:220px;border:1px solid white"/></td>
				<td style="height:10px"><input type="text" name="telephone_number" style="width:160px;border:1px solid white"/></td>
				
				</tr>
				<tr style="height:10px">
				<td style="height:10px"><input type="text" name="nameRelationship" style="width:160px;border:1px solid white"/></td>
				<td style="height:10px"><input  type="text" name="relationship_people" style="width:160px;border:1px solid white"/></td>
				<td style="height:10px"><input type="text" name="work_unit" style="width:220px;border:1px solid white"/></td>
				<td style="height:10px"><input  type="text" name="stay_place" style="width:220px;border:1px solid white" /></td>
				<td style="height:10px"><input type="text" name="telephone_number" style="width:160px;border:1px solid white"/></td>
				
				</tr>
				<tr style="height:10px">
				<td style="height:10px"><input type="text" name="nameRelationship" style="width:160px;border:1px solid white"/></td>
				<td style="height:10px"><input  type="text" name="relationship_people" style="width:160px;border:1px solid white"/></td>
				<td style="height:10px"><input type="text" name="work_unit" style="width:220px;border:1px solid white"/></td>
				<td style="height:10px"><input  type="text" name="stay_place" style="width:220px;border:1px solid white"/></td>
				<td style="height:10px"><input type="text" name="telephone_number" style="width:160px;border:1px solid white"/></td>
				
				</tr>
				</table>
			</div>
		</div>
		<p id="title">出所信息</p>
		<table>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">出所日期：</label>
			<div class="controls">
				<input name="outDate" type="text"  maxlength="20" class="input-medium Wdate wr required"
					value="<fmt:formatDate value="${atcBase.outDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">去向：</label>
			<div class="controls">
				<form:input path="goDirection" htmlEscape="false" maxlength="10" class="input-xlarge required"/>
				
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">填表人：</label>
			<div class="controls">
				<form:input path="writeTablePeople" htmlEscape="false" maxlength="8" class="input-xlarge wr required"/>
				
			</div>
		</div>
		</td>
		</tr>
		</table>
		<div class="control-group">
			<label class="control-label">出所原因：</label>
			<div class="controls">
				<form:textarea path="outReason" htmlEscape="false" rows="4" maxlength="200" class="input-xxlarge area required"  placeholder="请输入1-20个字"/>
			
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="atcbase:atcBase:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
		</div>
	</form:form>
</body>
</html>