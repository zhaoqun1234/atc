<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>保存基本信息成功管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<style>
	 .write{
	   width:100px;
	  }
	</style>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/atcbase/atcBase/">人员基本信息查询</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="atcBase" action="${ctx}/atcbase/atcBase/drugCategory" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li style="width:500px">
		<label>戒毒类型:</label>
		<form:select path="category" class="input-xlarge required write">
				<form:option value="" label="请选择"/>
					<form:option value="自愿" label="自愿"/>
					<form:option value="强制" label="强制"/>
					<form:options items="${fns:getDictList('')}" itemLabel="label" itemValue="value" htmlEscape="false" id="write"/>
				</form:select>
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				</li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
			   <th>编号</th>
				<th>姓名</th>
				<th>别名</th>
				<th>性别</th>
				<th>民族</th>
				<th>出生日期</th>
				<th>文化程度</th>
				<th>婚姻状态</th>
				<th>国籍</th>
				<th>籍贯</th>
				<th>证件号码</th>
				<th>出所日期</th>
				
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="atcBase">
			<tr>
				<td>${atcBase.no}</td>
				<td><a href="${ctx}/atcbase/atcBase/lookMessage?id=${atcBase.id}">
					${atcBase.name}
				</a></td>
				<td>${atcBase.alias}</td>
			
				<td>${atcBase.sex}</td>
				<td>${atcBase.nation}</td>
				<td><fmt:formatDate value="${atcBase.birthdate}" pattern="yyyy-MM-dd"/></td>
				<td>${atcBase.culture}</td>
				<td>${atcBase.marital}</td>
				<td>${atcBase.country}</td>
				<td>${atcBase.nativePlace}</td>
			    <td>${atcBase.idcardNumber}</td>
				<td><fmt:formatDate value="${atcBase.outDate}" pattern="yyyy-MM-dd"/></td>
				<%-- <shiro:hasPermission name="atcbase:atcBase:edit"><td>
    				<a href="${ctx}/atcbase/atcBase/lookMessage?id=${atcBase.id}">修改</a>
					<a href="${ctx}/atcbase/atcBase/delete?id=${atcBase.id}" onclick="return confirmx('确认要删除该条基本信息吗？', this.href)">删除</a>
				   <a href="${ctx}/atcbase/atcBase/lookMessage?id=${atcBase.id}">查看</a>
				</td></shiro:hasPermission> --%>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>