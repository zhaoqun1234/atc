<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<meta name="decorator" content="default"/>
	<link type="text/css" rel="stylesheet" href="${ctxStatic}/css/atc/atcBasePrintView.css" />
	<script type="text/javascript">
			function printdiv(printpage) 
			{ 
			var headstr = "<html><head><title></title></head><body>"; 
			var footstr = "</body>"; 
			var newstr = document.all.item(printpage).innerHTML; 
			var oldstr = document.body.innerHTML; 
			document.body.innerHTML = headstr+newstr+footstr; 
			window.print(); 
			document.body.innerHTML = oldstr; 
			return false; 
	} 
	
	</script>
	
	<style type="text/css">
	.btn-primary{
		margin-left: 300px;
		margin-top: 50px;
	}
	</style>
</head>
<body>
	  

 
	<div id="div_print"> 
 
	
	<div><sys:message content="${message}"/></div>
	
	
	
	<div class="div_basic">
			<div class="div_01">
				<div class="div_01_label"><label class="label01">人员编号:</label></div>
				<div class="div_01_input"><input type="text" name="no" value="${atcBase.no}" class="input01"/></div>
			</div>
			<!-- 人员基本信息 -->
			<div class="div_02">
				<div class="div_0201"><span class="span01">人员基本信息</span></div>
				<div class="div_0202">
					<div class="labelInput">
						<div class="div_label"><label class="label01">姓名:</label></div>
						<div class="div_input"><input type="text" name="name" value="${atcBase.name}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">别名:</label></div>
						<div class="div_input"><input type="text" name="alias" value="${atcBase.alias}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">性别:</label></div>

						<div class="div_input"><input type="text" name="sex"  value="${atcBase.sex}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">出生日期:</label></div>
						<div class="div_input"><input type="text" name="birthdate" value="<fmt:formatDate value="${atcBase.birthdate}" pattern="yyyy-MM-dd"/>" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">婚姻状况:</label></div>
						<div class="div_input"><input type="text" name="marital" value="${atcBase.marital}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">民族:</label></div>
						<div class="div_input"><input type="text" name="nation" value="${atcBase.nation}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">文化程度:</label></div>
						<div class="div_input"><input type="text" name="culture" value="${atcBase.culture}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">证件号码:</label></div>
						<div class="div_input"><input type="text" name="idcardNumber" value="${atcBase.idcardNumber}" class="input01"/></div>
					</div>
				</div>
				<%-- <div class="div_0203">
					<div class="div_020301">图像:</div>
					<div class="div_020302"><img alt="" src="${atcBase.imagePeople}" /></div>
				</div> --%>
				<div class="div_0204">
					<div class="labelInput">
						<div class="div_label"><label class="label01">国籍:</label></div>
						<div class="div_input"><input type="text" name="country" value="${atcBase.country}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">籍贯:</label></div>
						<div class="div_input"><input type="text" name="nativePlace" value="${atcBase.nativePlace}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">职业:</label></div>
						<div class="div_input"><input type="text" name="job" value="${atcBase.job}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">户口所在地:</label></div>
						<div class="div_input"><input type="text" name="address" value="${atcBase.address}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">现居地:</label></div>
						<div class="div_input"><input type="text" name="nowPlace" value="${atcBase.nowPlace}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">工作单位:</label></div>
						<div class="div_input"><input type="text" name="workUnit" value="${atcBase.workUnit}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">特征体表标记:</label></div>
						<div class="div_input"><input type="text" name="specialSigns" value="${atcBase.specialSigns}" class="input01"/></div>
					</div>
				</div>
			</div>
			<!-- 吸毒信息 -->
			<div class="div_03">
				<div class="div_0301"><span class="span01">吸毒信息</span></div>
				<div class="div_0302">
					<div class="labelInput">
						<div class="div_label"><label class="label01">初次吸毒时间:</label></div>
						<div class="div_input"><input type="text" name="fristDate" value="${atcBase.fristDate}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">已往吸毒次数:</label></div>
						<div class="div_input"><input type="text" name="times" value="${atcBase.times}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">已往戒毒次数:</label></div>
						<div class="div_input"><input type="text" name="timesGiveupDrug" value="${atcBase.timesGiveupDrug}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">吸毒方式:</label></div>
						<div class="div_input"><input type="text" name="ways" value="${atcBase.ways}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">强制戒毒期限:</label></div>
						<div class="div_input"><input type="text" name="starttime"  value="<fmt:formatDate value="${atcBase.starttime}" pattern="yyyy-MM-dd"/>" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label_01"><label class="label01">至:</label></div>
						<div class="div_input"><input type="text" name="endtime" value="<fmt:formatDate value="${atcBase.endtime}" pattern="yyyy-MM-dd"/>" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">吸毒种类:</label></div>
						<div class="div_input"><input type="text" name="drugVarieties" value="${atcBase.drugVarieties}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label_02"><label class="label01">决定强制戒毒的公安机关:</label></div>
						<div class="div_input"><input type="text" name="police" value="${atcBase.police}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">联系电话:</label></div>
						<div class="div_input"><input type="text" name="telephone" value="${atcBase.telephone}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">入所日期:</label></div>
						<div class="div_input"><input type="text" name="inDate"  value="<fmt:formatDate value="${atcBase.inDate}" pattern="yyyy-MM-dd"/>" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">其他并行处罚:</label></div>
						<div class="div_input"><input type="text" name="punishMoney" value="${atcBase.punishMoney}" class="input01_03"/></div>
						<!-- <div class="div_label"><label class="label01">（万元）</label></div> -->
					</div>
					<div class="labelInput">
						<div class="div_label_03"><label class="label01">（万元）办案单位:</label></div>
						<div class="div_input"><input type="text" name="handleUntis" value="${atcBase.handleUntis}" class="input01_04"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">病房号:</label></div>
						<div class="div_input"><input type="text" name="bedNumber" value="${atcBase.bedNumber}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">医护人员:</label></div>
						<div class="div_input"><input type="text" name="medicalWork" value="${atcBase.medicalWork}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">入所分类:</label></div>
						<div class="div_input"><input type="text" name="category" value="${atcBase.category}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">人员分类:</label></div>
						<div class="div_input"><input type="text" name="personClassification" value="${atcBase.personClassification}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">管教民警:</label></div>
						<div class="div_input"><input type="text" name="policeDisciline" value="${atcBase.policeDisciline}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">协管民警:</label></div>
						<div class="div_input"><input type="text" name="assistPolice" value="${atcBase.assistPolice}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">戒毒费:</label></div>
						<div class="div_input"><input type="text" name="drugFee" value="${atcBase.drugFee}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label"><label class="label01">零用钱:</label></div>
						<div class="div_input"><input type="text" name="pocketMoney" value="${atcBase.pocketMoney}" class="input01"/></div>
					</div>
				<%-- 	<div class="labelInput_02">
						<div class="div_label_04"><label class="label01">何时何地经过和机关处理:</label></div>
						<div class="div_textarea"><textarea rows="3" cols="95" name="officeDealWith">${officeDealWith}</textarea></div>
						<!-- <div class="div_input"><input type="text" class="input01_05"/></div>
						<div class="div_input"><input type="text" class="input01_06"/></div> -->
					</div>
					<div class="labelInput_02">
						<div class="div_label_04"><label class="label01">戒毒人员个人简历:</label></div>
						<div class="div_textarea"><textarea rows="3" cols="95" name="drugPeopleIntroduce">${drugPeopleIntroduce}</textarea></div>
						<!-- <div class="div_input"><input type="text" class="input01_05"/></div>
						<div class="div_input"><input type="text" class="input01_06"/></div> -->
					</div> --%>
				</div>
			</div>
			<!-- 家庭成员及社会关系 -->
		<%-- 	<div class="div_04">
				<div class="div_0401"><span class="span01">家庭成员及社会关系</span></div>
				<div class="div_0402">
					<table>
						<tr>
							<th>姓名</th>
							<th> 与此人关系</th>
							<th>住所</th>
							<th>联系电话</th>
							<th>工作单位</th>
						</tr>
						<c:forEach items="${relationship}" var="AtcSocietyRelationship">
						<tr>
						
							<td>${AtcSocietyRelationship.nameRelationship }</td>
							<td>${AtcSocietyRelationship.relationshipPeople } </td>
							<td>${AtcSocietyRelationship.workUnit }</td>
							<td>${AtcSocietyRelationship.stayPlace }</td>
							<td>${AtcSocietyRelationship.telephoneNumber }</td>
						</tr>
						</c:forEach>
					</table>
				</div>
			</div> --%>
			<!-- 出所信息 -->
			<div class="div_05">
				<div class="div_0501"><span class="span01">出所信息</span></div>
				<div class="div_0502">
					<div class="labelInput">
						<div class="div_label_05"><label class="label01">出所日期:</label></div>
						<div class="div_input"><input type="text" name="outDate" value="<fmt:formatDate value="${atcBase.outDate}" pattern="yyyy-MM-dd"/>" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label_06"><label class="label01">原因:</label></div>
						<div class="div_input"><input type="text" name="outReason" value="${atcBase.outReason}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label_06"><label class="label01">去向:</label></div>
						<div class="div_input"><input type="text" name="goDirection" value="${atcBase.goDirection}" class="input01"/></div>
					</div>
					<div class="labelInput">
						<div class="div_label_05"><label class="label01">填表人:</label></div>
						<div class="div_input"><input type="text" name="writeTablePeople" value="${atcBase.writeTablePeople}" class="input01"/></div>
					</div>
				</div>
			</div>
			<%--打印、返回 按钮 预览时不需要
				给class设置个height=50px--页面底部留白 --%>
			<div class="div_06">
				<!-- <div class="div_0601">
					<input type="button" value="返回" class="btn01"/>
					<input type="button" value="打印" class="btn02"/>
				</div> -->
			</div>
		</div>
	
	
 
</div> 
	<input name="b_print" type="button" class="btn btn-primary" onClick="printdiv('div_print');" value=" 打印 " > 
</body>
</html>