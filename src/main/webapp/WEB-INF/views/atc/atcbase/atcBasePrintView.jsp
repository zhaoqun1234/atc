<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<!-- <meta name="decorator" content="default"/> -->
		<title>戒毒人员信息打印预览</title>
		
	
		<script type="text/javascript">
			function printdiv(printpage) 
			{ 
			var headstr = "<html><head><title></title></head><body>"; 
			var footstr = "</body>"; 
			var newstr = document.all.item(printpage).innerHTML; 
			var oldstr = document.body.innerHTML; 
			document.body.innerHTML = headstr+newstr+footstr; 
			window.print(); 
			document.body.innerHTML = oldstr; 
			return false; 
	} 
	
	</script>
	
 	<style type="text/css">
	.btn-primary{
		margin-left: 800px;
		margin-top: 50px;
	} 
	</style>
	<style>
	table{
	
	width:850px;
	border:1px solid #F5F5F5;
	}
	td{
	border:1px solid #F5F5F5;
	height:20px;
	 text-align:center;
	}
	tr{
	
	height:40px}
	.td1{
	width:100px;
	}
	.td12{
	width:100px;
	}
	.td13{
	width:80px;
	}.td3{
	width:80px}
	.td4{
	width:130px}
	.td44{
	width:80px}
	#size{
	font-size:15px;font-weight:bold;
	}
	</style>
	</head>
	<body>

	<div id="div_print"> 
	  <table align="center" cellpadding="0" cellspacing="0">
	<tr><td style="font-size:20px;font-weight:bold">人员编号 </td><td style="border:0"> ${page.no }</td></tr>
	<tr><td id="size">人员基本信息</td></tr>
	<tr><td class="td1">姓名</td><td class="td12">${page.name }</td><td class="td13">别名</td><td class="td3">${page.alias }</td><td class="td4">民族</td><td class="td44">${page.nation }</td><td style="border:0"></td></tr>
	<tr><td class="td1">出生日期</td><td class="td12"><fmt:formatDate value="${page.birthdate }" pattern="yyyy-MM-dd"/></td><td class="td13">婚姻状况</td><td>${page.marital }</td><td class="td4">性别</td><td class="td44">${page.sex }</td><td style="border:0"></td></tr>
    <tr><td class="td1">文化程度</td><td class="td12">${page.culture }</td><td class="td13">证件号码</td><td colspan="3">${page.idcardNumber }</td><td style="border:0"></td></tr>
    <tr><td class="td1">国籍</td><td class="td12">${page.country }</td><td class="td13">籍贯</td><td>${page.nativePlace }</td><td>职业</td><td>${page.job }</td><td style="border:0"></td></tr>
    <tr><td class="td1">体表特征</td><td class="td12">${page.specialSigns }</td><td>工作单位</td><td colspan="3">${page.workUnit }</td></tr>
	<tr><td class="td1">户口所在地</td><td colspan="6">${page.address }</td></tr>
	<tr><td class="td1">现居地</td><td colspan="6">${page.nowPlace }</td></tr>
	</table>
	<table align="center" cellpadding="0" cellspacing="0">
	<tr><td id="size">吸毒信息</td></tr>
	<tr><td style="width:100px">初次吸毒时间</td><td style="width:120px"><fmt:formatDate value="${page.fristDate }" pattern="yyyy-MM-dd"/></td><td style="width:100px">以往戒毒次数</td><td style="width:100px">${page.times }</td><td style="width:80px">以往吸毒次数</td><td colspan="3">${page.timesGiveupDrug }</td></tr>
	<tr><td>吸毒方式</td><td>${page.ways }</td><td>吸毒种类</td><td>${page.drugVarieties }</td><td>决定强制戒毒公安机关</td><td colspan="3">${page.police }</td></tr>
	<tr><td>强制戒毒期限</td><td><fmt:formatDate value="${page.starttime }" pattern="yyyy-MM-dd"/></td><td>至</td><td><fmt:formatDate value="${page.endtime }" pattern="yyyy-MM-dd"/></td><td>其他并行处罚</td><td colspan="3">${page.punishMoney }</td></tr>
	<tr><td>联系电话</td><td>${page.telephone }</td><td>入所日期</td><td><fmt:formatDate value="${page.inDate }" pattern="yyyy-MM-dd"/></td><td>办案单位</td><td colspan="3">${page.handleUntis }</td></tr>
	<tr><td>病床号</td><td>${page.bedNumber }</td><td>医护人员</td><td>${page.medicalWork }</td><td>入所分类</td><td>${page.category }</td><td>人员分类</td><td>${page.personClassification }</td></tr>
	<tr><td>管教民警</td><td>${page.policeDisciline }</td><td>协管民警</td><td>${page.assistPolice }</td><td>戒毒费</td><td>${page.drugFee }</td><td>零用钱</td><td>${page.pocketMoney }</td></tr>
	<tr><td rowspan="2">何时何地经过何机关处罚</td><td colspan="7" rowspan="2">${page.officeDealWith }</td></tr>
	<tr></tr>
	<tr><td rowspan="2">戒毒人员简历</td><td colspan="7" rowspan="2">${page.drugPeopleIntroduce }</td></tr>
	<tr></tr>
	</table>
	<table align="center" cellpadding="0" cellspacing="0">
	<tr><td id="size">家庭成员及关系</td></tr>
	<tr><td style="width:125px">姓名</td><td style="width:100px">与此人关系</td><td style="width:220px">工作单位</td><td style="width:220px">住所</td><td>联系电话</td></tr>
	<c:forEach items="${relationship}" var="relationship">
	<tr><td>${relationship.nameRelationship }</td><td>${relationship.relationshipPeople }</td><td>${relationship.workUnit }</td><td>${relationship.stayPlace }</td><td>${relationship.telephoneNumber }</td></tr>
	</c:forEach>
	</table>
	<table align="center" cellpadding="0" cellspacing="0"> 
	<tr><td id="size">出所信息</td></tr>
	<tr><td style="width:124px">出所日期</td><td style="width:200px"><fmt:formatDate value="${page.outDate }" pattern="yyyy-MM-dd"/></td><td style="width:120px">去向 </td><td>${page.goDirection }</td></tr>
	<tr><td>原因</td><td colspan="3">${page.outReason }</td></tr>
	
	<tr><td></td><td></td><td>填表人</td><td>${page.writeTablePeople }</td></tr>
	</table>
	<br>
	</div>
	<div style="margin:auto;width:100px">
	<input name="b_print" type="button"  onClick="printdiv('div_print');" value=" 打印 " style="background-color:#39abe8;width:60px;height:30px">
	</div>
	</body>
	</html>