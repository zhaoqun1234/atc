<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>保存基本信息成功管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/atcbase/atcBase/">人员基本信息查询</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="atcBase" action="${ctx}/atcbase/atcBase/peopleSee" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
		<li>
		
		<label class="control-label">病床号：</label>
		 <form:input path="bedNumber" htmlEscape="false" maxlength="100" class="input-xlarge" style="width:100px"/>
		
		</li>
		<li>
		
		<label class="control-label">主管民警：</label>
		 <form:input path="policeDisciline" htmlEscape="false" maxlength="100" class="input-xlarge" style="width:100px"/>
		
		</li>
		<li>
		
		<label class="control-label">斜管民警：</label>
		 <form:input path="assistPolice" htmlEscape="false" maxlength="100" class="input-xlarge" style="width:100px"/>
		
		</li>
		<li>
		
		</li>
		<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>姓名</th>
				<th>别名</th>
				<th>编号</th>
				<th>性别</th>
				<th>民族</th>
				<th>出生日期</th>
				<th>文化程度</th>
				<th>婚姻状态</th>
				<th>国籍</th>
				<th>籍贯</th>
				<th>证件号码</th>
				<th>出所日期</th>
			
				
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="atcBase">
			<tr>
				<td><a href="${ctx}/atcbase/atcBase/lookMessage?id=${atcBase.id}">
					${atcBase.name}
				</a></td>
				<td>${atcBase.alias}</td>
				<td>${atcBase.no}</td>
				<td>${atcBase.sex}</td>
				<td>${atcBase.nation}</td>
				<td><fmt:formatDate value="${atcBase.birthdate}" pattern="yyyy-MM-dd"/></td>
				<td>${atcBase.culture}</td>
				<td>${atcBase.marital}</td>
				<td>${atcBase.country}</td>
				<td>${atcBase.nativePlace}</td>
			    <td>${atcBase.idcardNumber}</td>
				<td><fmt:formatDate value="${atcBase.outDate}" pattern="yyyy-MM-dd"/></td>
			
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>