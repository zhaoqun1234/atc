<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>保存基本信息成功管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/atcbase/atcSocietyRelationship/">保存基本信息成功列表</a></li>
		<shiro:hasPermission name="atcbase:atcSocietyRelationship:edit"><li><a href="${ctx}/atcbase/atcSocietyRelationship/form">保存基本信息成功添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="atcSocietyRelationship" action="${ctx}/atcbase/atcSocietyRelationship/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>姓名：</label>
				<form:input path="name" htmlEscape="false" maxlength="25" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>姓名</th>
				<th>修改时间</th>
				<shiro:hasPermission name="atcbase:atcSocietyRelationship:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="atcSocietyRelationship">
			<tr>
				<td><a href="${ctx}/atcbase/atcSocietyRelationship/form?id=${atcSocietyRelationship.id}">
					${atcSocietyRelationship.name}
				</a></td>
				<td>
					<fmt:formatDate value="${atcSocietyRelationship.updateDate}" pattern="yyyy-MM-dd"/>
				</td>
				<shiro:hasPermission name="atcbase:atcSocietyRelationship:edit"><td>
    				<a href="${ctx}/atcbase/atcSocietyRelationship/form?id=${atcSocietyRelationship.id}">修改</a>
					<a href="${ctx}/atcbase/atcSocietyRelationship/delete?id=${atcSocietyRelationship.id}" onclick="return confirmx('确认要删除该保存基本信息成功吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>