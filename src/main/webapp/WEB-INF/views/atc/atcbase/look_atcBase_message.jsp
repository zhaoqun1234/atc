<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>保存基本信息成功管理</title>
	<meta name="decorator" content="default"/>
	 <script type="text/javascript">
		$(document).ready(function() {
			
		
			$("#inputForm").validate({
				 submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},  
			 	 errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}  
			}); 
		}); 
	</script>
	<style>
	 table{
	 width:1600px;
	
	 }
	 td{
	 width:2000px;
	 
	 }
	 #write{
	 width:150px
	 }
	 .write{
	 width:160px
	 }
	 .area{
	 
	 width:1400px}
	 #title{
	 font-size:20px;font-weight:bold;height:60px
	 }
	 #relationship input{
	 width:150px;
	 }
	</style>
</head>
<body>
	<ul class="nav nav-tabs">
		
		<li class="active"><a href="${ctx}/atcbase/atcBase/form?id=${atcBase.id}">人员基本信息查看</a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="atcBase" action="${ctx}/atcbase/atcBase/updateMessage" method="post" class="form-horizontal">
		 <form:hidden path="id"/> 
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label" id="title">人员编号：</label>
			<div class="controls">
				<form:input path="no" htmlEscape="false" maxlength="255" class="input-xlarge required" readonly="true" id="write"/>
				
			</div>
		</div>
		
		 
		
		
		<table>
		<tr ><td id="title">人员基本信息查看</td></tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">姓&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名：</label>
			<div class="controls">
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">别名：</label>
			<div class="controls">
				<form:input path="alias" htmlEscape="false" maxlength="255" class="input-xlarge " id="write" readonly="true"/>
			</div>
		</div>
		</td>
		<td>
			<div class="control-group">
			<label class="control-label">性别：</label>
			<div class="controls">
				<form:select path="sex" style="width:170px" required="required">
				<form:option value="" label="请选择"/>
					<form:option value="男" label="男"/>
					<form:option value="女" label="女"/>
					<form:option value="其他" label="其他"/>
					<form:options items="${fns:getDictList('')}" itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			
			</div>
		</div>
		</td>
		<td>
	<div class="control-group">
			<label class="control-label">图像：</label>
			<div class="controls">
				<form:hidden id="imagePeople" path="imagePeople" htmlEscape="false" maxlength="1000" class="input-xlarge" readonly="true"/>
				<sys:ckfinder input="imagePeople" type="images" uploadPath="/atcbase/atcBase" selectMultiple="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">婚姻状态：</label>
			<div class="controls">
				<form:select path="marital" class="input-xlarge required write">
				<form:option value="" label="请选择"/>
					<form:option value="已婚" label="已婚"/>
					<form:option value="未婚" label="未婚"/>
					<form:option value="离异" label="离异"/>
					<form:option value="其他" label="其他"/>
					<form:options items="${fns:getDictList('marital')}" itemLabel="label" itemValue="value" htmlEscape="false" readonly="true"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		
			<div class="control-group">
			<label class="control-label">出生日期：</label>
			<div class="controls">
				<input name="birthdate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required" id="write"
					value="<fmt:formatDate value="${atcBase.birthdate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">民族：</label>
			<div class="controls">
				<form:input path="nation" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">文化程度：</label>
			<div class="controls">
				<form:select path="culture" class="input-xlarge write">
					<form:option value="" label="请选择"/>
					<form:option value="文盲" label="文盲"/>
					<form:option value="小学" label="小学"/>
					<form:option value="初中" label="初中"/>
					<form:option value="高中" label="高中"/>
					<form:option value="大专" label="大专"/>
					<form:option value="本科及以上" label="本科及以上"/>
					<form:options items="${fns:getDictList('culture')}" itemLabel="label" itemValue="value" htmlEscape="false" id="write" readonly="true"/>
				</form:select>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">证件号码：</label>
			<div class="controls">
				<form:input path="idcardNumber" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">国籍：</label>
			<div class="controls">
				<form:input path="country" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">籍贯：</label>
			<div class="controls">
				<form:input path="nativePlace" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">职业：</label>
			<div class="controls">
				<form:input path="job" htmlEscape="false" maxlength="255" class="input-xlarge " id="write" readonly="true"/>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">户口所在地：</label>
			<div class="controls">
				<form:input path="address" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">现居地：</label>
			<div class="controls">
				<form:input path="nowPlace" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">工作单位：</label>
			<div class="controls">
				<form:input path="workUnit" htmlEscape="false" maxlength="255" class="input-xlarge " id="write" readonly="true"/>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">特殊体征：</label>
			<div class="controls">
				<form:input path="specialSigns" htmlEscape="false" maxlength="255" class="input-xlarge " id="write" readonly="true"/>
			</div>
		</div></td>
		</tr>
		</table>
		<table>
		<tr><td id="title">吸&nbsp;&nbsp;&nbsp;毒&nbsp;&nbsp;&nbsp;信&nbsp;&nbsp;&nbsp;息</td></tr>
		 <tr>
		 <td>
		<div class="control-group">
			<label class="control-label">初次吸毒时间：</label>
			<div class="controls">
				<input name="fristDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required" id="write" 
					value="<fmt:formatDate value="${atcBase.fristDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">以往吸毒次数：</label>
			<div class="controls">
				<form:input path="times" htmlEscape="false" maxlength="3" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">以往戒毒次数：</label>
			<div class="controls">
				<form:input path="timesGiveupDrug" htmlEscape="false" maxlength="3" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">吸食方式：</label>
			<div class="controls">
				<form:input path="ways" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">强制戒毒起始日期：</label>
			<div class="controls">
				<input name="starttime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required" id="write"
					value="<fmt:formatDate value="${atcBase.starttime}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">强制戒毒终止日期：</label>
			<div class="controls">
				<input name="endtime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required" id="write"
					value="<fmt:formatDate value="${atcBase.endtime}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">吸毒种类：</label>
			<div class="controls">
				<form:input path="drugVarieties" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">强制戒毒公安单位：</label>
			<div class="controls">
				<form:select path="police" class="input-xlarge required write">
				<form:option value="" label="请选择"/>
					<form:option value="威海市公安局刑事侦查支队" label="威海市公安局刑事侦查支队"/>
					<form:option value="威海市看守所" label="威海市看守所"/>
					<form:option value="威海市公安局经济技术开发区分局" label="威海市公安局经济技术开发区分局"/>
					<form:option value="威海市公安局" label="威海市公安局"/>
					<form:option value="其他" label="其他"/>
					<form:options items="${fns:getDictList('police')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">公安机关联系电话：</label>
			<div class="controls">
				<form:input path="telephone" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">入所日期：</label>
			<div class="controls">
				<input name="inDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required" id="write"
					value="<fmt:formatDate value="${atcBase.inDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">其他并行处罚：</label>
			<div class="controls">
				<form:input path="punishMoney" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">办案单位：</label>
			<div class="controls">
				<form:input path="handleUntis" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">病床号：</label>
			<div class="controls">
				<form:input path="bedNumber" htmlEscape="false" maxlength="100" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">医护人员：</label>
			<div class="controls">
				<form:input path="medicalWork" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">入所分类：</label>
			<div class="controls">
				<form:select path="category" class="input-xlarge required write" readonly="true">
				<form:option value="" label="请选择"/>
					<form:option value="自愿" label="自愿"/>
					<form:option value="强制" label="强制"/>
					<form:options items="${fns:getDictList('category')}" itemLabel="label" itemValue="value" htmlEscape="false" id="write"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">人员分类：</label>
			<div class="controls">
				<form:select path="personClassification" class="input-xlarge required write" readonly="true">
				<form:option value="" label="请选择"/>
					<form:option value="转拘人员" label="转拘人员"/>
					<form:option value="拟牢人员" label="拟牢人员"/>
					<form:options items="${fns:getDictList('')}" itemLabel="label" itemValue="value" htmlEscape="false" id="write"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">管教民警：</label>
			<div class="controls">
				<form:input path="policeDisciline" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">协管民警：</label>
			<div class="controls">
				<form:input path="assistPolice" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">戒毒费：</label>
			<div class="controls">
				<form:input path="drugFee" htmlEscape="false" maxlength="7" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">零用钱：</label>
			<div class="controls">
				<form:input path="pocketMoney" htmlEscape="false" maxlength="7" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		</table>
		<div class="control-group">
			<label class="control-label">何时何地经过机关处理：</label>
			<div class="controls">
				<form:textarea path="officeDealWith" htmlEscape="false" rows="4" maxlength="2000" class="input-xxlarge required area" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">戒毒人员个人简历：</label>
			<div class="controls">
				<form:textarea path="drugPeopleIntroduce" htmlEscape="false" rows="4" maxlength="2000" class="input-xxlarge required area" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div> 
		 <p id="title">家庭人员及关系:<p>
		
		<div class="control-group">
			<label class="control-label"></label>
			<div class="controls">
				<table id="relationship" style="width:1360px;border:1px solid #cccccc" class="Border">
				<tr style="height:10px">
				<td>姓名</td>
				<td>与该人员关系</td>
				<td>工作单位</td>
				<td>住所</td>
				<td>联系电话</td>
				
				</tr>
				<c:forEach items="${relationship}" var="AtcSocietyRelationship">
				<tr style="height:10px">
				<td style="height:10px"><input type="text" name="nameRelationship" style="width:200px;border:1px solid white" value="${AtcSocietyRelationship.nameRelationship }" readonly="readonly"/></td>
				<td style="height:10px"><input  type="text" name="relationship_people"style="width:200px;border:1px solid white" value="${AtcSocietyRelationship.relationshipPeople }" readonly="readonly"/></td>
				<td style="height:10px"><input type="text" name="work_unit" style="width:300px;border:1px solid white" value="${AtcSocietyRelationship.workUnit }" readonly="readonly"/></td>
				<td style="height:10px"><input  type="text" name="stay_place" style="width:300px;border:1px solid white" value="${AtcSocietyRelationship.stayPlace }" readonly="readonly"/></td>
				<td style="height:10px"><input type="text" name="telephone_number" style="width:200px;border:1px solid white" value="${AtcSocietyRelationship.telephoneNumber }" readonly="readonly"/></td>
				
				</tr>
				</c:forEach>
						</table>
			</div>
		</div>
		
		
		<p id="title">出&nbsp;&nbsp;&nbsp;所&nbsp;&nbsp;&nbsp;信&nbsp;&nbsp;&nbsp;息</p>
		<table>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">出所日期：</label>
			<div class="controls">
				<input name="outDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required" id="write"
					value="<fmt:formatDate value="${atcBase.outDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">去向：</label>
			<div class="controls">
				<form:input path="goDirection" htmlEscape="false" maxlength="255" class="input-xlarge required" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">填表人：</label>
			<div class="controls">
				<form:input path="writeTablePeople" htmlEscape="false" maxlength="255" class="input-xlarge required" id="write" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		</table>
		<div class="control-group">
			<label class="control-label">出所原因：</label>
			<div class="controls">
				<form:textarea path="outReason" htmlEscape="false" rows="4" maxlength="1000" class="input-xxlarge required area" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="form-actions">
			
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>


