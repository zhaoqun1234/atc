<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>保存基本信息成功管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<style>
	 .write{
	   width:200px;
	  }
	</style>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/atcbase/atcBase/">人员基本信息查询</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="atcBase" action="${ctx}/atcbase/atcBase/time" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form" style="height:60px">
		<li>
		<label class="control-label">入所日期：</label>
				<input name="inDate" type="text" maxlength="20" class="input-medium Wdate " 
					value="<fmt:formatDate value="${atcBase.inDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
		</li> 
		<li>
			<label class="control-label">出所日期：</label>
				<input name="outDate" type="text" maxlength="20" class="input-medium Wdate " 
					value="<fmt:formatDate value="${atcBase.outDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
		</li> 
		<li style="width:600px">
		<span >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;强制戒毒单位：</span>
		<form:select path="police" class="input-xlarge required write">
				<form:option value="" label="请选择"/>
					<form:option value="山东省威海市戒毒大队" label="山东省威海市戒毒大队"/>
					<form:option value="山东省威海市戒毒分队" label="山东省威海市戒毒分队"/>
					<form:option value="其他" label="其他"/>
					<form:options items="${fns:getDictList('police')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
		</li>
		
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>姓名</th>
				<th>别名</th>
				<th>编号</th>
				<th>性别</th>
				<th>民族</th>
				<th>出生日期</th>
				<th>文化程度</th>
				<th>婚姻状态</th>
				<th>国籍</th>
				<th>籍贯</th>
				<th>证件号码</th>
				<th>出所日期</th>
			
				
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="atcBase">
			<tr>
				<td><a href="${ctx}/atcbase/atcBase/lookMessage?id=${atcBase.id}">
					${atcBase.name}
				</a></td>
				<td>${atcBase.alias}</td>
				<td>${atcBase.no}</td>
				<td>${atcBase.sex}</td>
				<td>${atcBase.nation}</td>
				<td><fmt:formatDate value="${atcBase.birthdate}" pattern="yyyy-MM-dd"/></td>
				<td>${atcBase.culture}</td>
				<td>${atcBase.marital}</td>
				<td>${atcBase.country}</td>
				<td>${atcBase.nativePlace}</td>
			    <td>${atcBase.idcardNumber}</td>
				<td><fmt:formatDate value="${atcBase.outDate}" pattern="yyyy-MM-dd"/></td>
			
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>