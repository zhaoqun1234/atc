<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>体检表管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					var checkWeight=$("#checkWeight").val();
					var checkHeight=$("#checkHeight").val();
					var checkLongEnough=$("#checkLongEnough").val();
					if(""!=checkWeight){
				 if(!(/^\d{2,}$/i.test(checkWeight))) {
					 $("#infoweight").text("体重格式不正确");
					 return false;
				 }else{
					 $("#infoweight").text("*");
				 }
			} 
			/*身高校验*/
			if(""!=checkHeight){
				 if(!(/^\d{2,}$/i.test(checkHeight))) {
					 $("#infoHeight").text("身高格式不正确");
					 return false;
				 }else{
					 $("#infoHeight").text("*");
				 }
			} 
				/*足长校验*/	
				if(""!=checkLongEnough){
				 if(!(/^\d{2,}$/i.test(checkLongEnough))) {
					 $("#infoLongEnough").text("足长格式不正确");
					 return false;
				 }else{
					 $("#infoLongEnough").text("*");
				 }
			} 
					
					
					
					
					
					
					
					
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
	<style type="text/css">
#doc {
	resize: none;
	width: 900px;
}
</style>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/checks/atcCheck/">体检表列表</a></li>
		<li class="active"><a href="${ctx}/checks/atcCheck/form?id=${atcCheck.id}">体检表<shiro:hasPermission name="checks:atcCheck:edit">${not empty atcCheck.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="checks:atcCheck:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="atcCheck" action="${ctx}/checks/atcCheck/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table>	
		<tr>
		<td>
		<div class="control-group" style="margin-left:20px;">
						<label class="control-label">检查编号：&nbsp;&nbsp;&nbsp;&nbsp;</label>
						<sys:treeselect id="checkId" name="checkId"
							value="${atcCheck.checkId}" labelName="no"
							labelValue="${atcBase.no}" title="用户"
							url="/atcbase/atcBase/treeData?type=3" cssClass="input-small required"
							allowClear="true" notAllowSelectParent="true" />
							<span class="help-inline"><font color="red" id="checkId">*</font> </span>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">体重：</label>
			<div class="controls">
				<form:input path="checkWeight" htmlEscape="false" maxlength="64" class="input-xlarge required" style="width:155px;" />
				<span class="help-inline" id="infoweight"><font color="red">*</font></span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">身高：</label>
			<div class="controls">
				<form:input path="checkHeight" htmlEscape="false" maxlength="64" class="input-xlarge required" style="width:155px;"/>
				<span class="help-inline" id="infoHeight"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">足长：</label>
			<div class="controls">
				<form:input path="checkLongEnough" htmlEscape="false" maxlength="64" class="input-xlarge required" style="width:155px;"/>
				<span class="help-inline" id="infoLongEnough"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">有无残疾：</label>
			<div class="controls">
				<form:input path="checkDisabled" htmlEscape="false" maxlength="64" class="input-xlarge required" style="width:155px;"/>
				<span class="help-inline"><font color="red">*</font></span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">曾患何种疾病：</label>
			<div class="controls">
				<form:input path="checkDisease" htmlEscape="false" maxlength="20" class="input-xlarge " style="width:155px;"/>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
						<label class="control-label">血型：</label>
						<div class="controls">
							<form:select path="checkBloodType" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="A" label="A" />
								<form:option value="B" label="B" />
								<form:option value="AB" label="AB" />
								<form:option value="O" label="O" />
								<form:option value="其它" label="其它" />
								<form:options items="${fns:getDictList('checkBloodPressure')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
						<label class="control-label">血压：</label>
						<div class="controls">
							<form:select path="checkBloodPressure" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkBloodPressure')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">血压不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkBloodPressureRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize:none;"/>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
						<label class="control-label">肺部：</label>
						<div class="controls">
							<form:select path="checkLungs" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkLungs')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">肺部不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkLungsRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
						<label class="control-label">心脏：</label>
						<div class="controls">
							<form:select path="checkHeart" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkHeart')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">心脏不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkHeartRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;" />
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
						<label class="control-label">腹部：</label>
						<div class="controls">
							<form:select path="checkAbdomen" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkAbdomen')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">腹部不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkAbdomenRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
						<label class="control-label">四肢关节：</label>
						<div class="controls">
							<form:select path="checkLimbs" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkLimbs')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">四肢关节不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkLimbsRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
						<label class="control-label">脊柱：</label>
						<div class="controls">
							<form:select path="checkSpine" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkSpine')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">脊柱不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkSpineRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
						<label class="control-label">胸透：</label>
						<div class="controls">
							<form:select path="checkThoracotomy" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkThoracotomy')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">胸透不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkThoracotomyRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
						<label class="control-label">心电图：</label>
						<div class="controls">
							<form:select path="checkElectrocardiogram" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options
									items="${fns:getDictList('checkElectrocardiogram')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">心电图不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkElectrocardiogramRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
						<label class="control-label">B超：</label>
						<div class="controls">
							<form:select path="checkBsuper" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkBsuper')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">B超不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkBsuperRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
						<label class="control-label">尿常规：</label>
						<div class="controls">
							<form:select path="checkUrineRoutine" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkUrineRoutine')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">尿常规不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkUrineRoutineRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
						<label class="control-label">血常规：</label>
						<div class="controls">
							<form:select path="checkBloodRoutine" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkBloodRoutine')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">血常规不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkBloodRoutineRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
						<label class="control-label">皮肤：</label>
						<div class="controls">
							<form:select path="checkSkin" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkSkin')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">皮肤不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkSkinRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
						<label class="control-label">体表损伤：</label>
						<div class="controls">
							<form:select path="checkBodyDamage" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkBodyDamage')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">体表损伤不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkBodyDamageRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
						<label class="control-label">肝功情况：</label>
						<div class="controls">
							<form:select path="checkLiverFunction" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="正常" label="正常" />
								<form:option value="不正常" label="不正常" />
								<form:options items="${fns:getDictList('checkLiverFunction')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">肝功情况不正常备注：</label>
			<div class="controls">
				<form:textarea path="checkLiverFunctionRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
						<label class="control-label">性病情况：</label>
						<div class="controls">
							<form:select path="checkSexuallyTransmittedDiseases" class="input-xlarge required"
								style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="有" label="有" />
								<form:option value="没有" label="没有" />
								<form:options
									items="${fns:getDictList('checkSexuallyTransmittedDiseases')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">性病备注：</label>
			<div class="controls">
				<form:textarea path="checkSexuallyTransmittedDiseasesRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		
		</td>
		<td>
		<div class="control-group">
						<label class="control-label">急性病情况：</label>
						<div class="controls">
							<form:select path="checkAcuteDisease" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="有" label="有" />
								<form:option value="没有" label="没有" />
								<form:options items="${fns:getDictList('checkAcuteDisease')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">急性病情况备注：</label>
			<div class="controls">
				<form:textarea path="checkAcuteDiseaseRemark" htmlEscape="false" maxlength="40" class="input-xlarge " style="width:155px;resize: none;"/>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
						<label class="control-label">艾滋病情况：</label>
						<div class="controls">
							<form:select path="checkAidsSituation" class="input-xlarge required" style="width:170px">
								<form:option value="" label="请选择" />
								<form:option value="有" label="有" />
								<form:option value="没有" label="没有" />
								<form:options items="${fns:getDictList('checkAidsSituation')}"
									itemLabel="label" itemValue="value" htmlEscape="false" />
							</form:select>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">检查日期：</label>
			<div class="controls">
				<input name="checkDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required"
					value="<fmt:formatDate value="${atcCheck.checkDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
					<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		</table>
		<div class="control-group">
			<label class="control-label">曾做过何种手术或外伤史：</label>
			<div class="controls">
				<form:textarea path="checkHistoryofsurgery" htmlEscape="false"
					rows="4" maxlength="200" class="input-xlarge" id="doc" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">医生建议：</label>
			<div class="controls">
				<form:textarea path="checkDoctorAdvises" htmlEscape="false" rows="4"
					maxlength="200" class="input-xlarge " id="doc" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">领导建议：</label>
			<div class="controls">
				<form:textarea path="checkLeadershipAdvice" htmlEscape="false"
					rows="4" maxlength="200" class="input-xlarge " id="doc" />
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="checks:atcCheck:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>