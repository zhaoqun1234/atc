<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
<title>评分标准管理</title>
<meta name="decorator" content="default" />
<script type="text/javascript">
	$(document).ready(
			function() {
				//$("#name").focus();
				$("#inputForm")
						.validate(
								{
									submitHandler : function(form) {
										loading('正在提交，请稍等...');
										form.submit();
									},
									errorContainer : "#messageBox",
									errorPlacement : function(error, element) {
										$("#messageBox").text("输入有误，请先更正。");
										if (element.is(":checkbox")
												|| element.is(":radio")
												|| element.parent().is(
														".input-append")) {
											error.appendTo(element.parent()
													.parent());
										} else {
											error.insertAfter(element);
										}
									}
								});
			});
</script>
<script type="text/javascript">
function jisuan(){
	var text1=document.getElementById("text1").value;
	var text2=document.getElementById("text2").value;
	if(text1<0||text1>100){
		document.getElementById("results").innerHTML="格式不正确！";
		document.getElementById("text1").value=0;
	}
	if(text2<0||text2>100){
		document.getElementById("resultse").innerHTML="格式不正确！";
		document.getElementById("text2").value=0;
	}
		document.getElementById("text3").value=parseFloat(text1)-parseFloat(text2);
}
</script>
<script type="text/javascript">

</script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/checks/atcAppraise/">评分标准列表</a></li>
		<li class="active"><a
			href="${ctx}/checks/atcAppraise/form?id=${atcAppraise.id}">评分标准<shiro:hasPermission
					name="checks:atcAppraise:edit">${not empty atcAppraise.id?'修改':'添加'}</shiro:hasPermission>
				<shiro:lacksPermission name="checks:atcAppraise:edit">查看</shiro:lacksPermission></a></li>
	</ul>
	<br />
	<form:form id="inputForm" modelAttribute="atcAppraise"
		action="${ctx}/checks/atcAppraise/save" method="post"
		class="form-horizontal">
		<form:hidden path="id" />
		<sys:message content="${message}" />
		<table>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">评分日期：</label>
			<div class="controls">
				<input name="appraiseDate" type="text" readonly="readonly"
					maxlength="20" class="input-medium Wdate required" style="width:100px;"
					value="<fmt:formatDate value="${atcAppraise.appraiseDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});" />
					<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">人员编号：</label>
			<div class="controls" >
				<sys:treeselect id="appraiseId" name="appraiseId"
					value="${atcAppraise.appraiseId}" labelName="input-small name"
					labelValue="${atcAppraise.name}" title="用户"
					url="/atcbase/atcBase/treeData?type=3" cssClass="required" 
					allowClear="true" notAllowSelectParent="true" disabled="disabled"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">加分：</label>
			<div class="controls">
				<form:input path="bonusPoint" type="text" maxlength="64"
					class="input-xlarge required" style="width:100px;" id="text1" onkeyup="jisuan()"/>
					<span class="help-inline" id="results" color="red"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">加分项目：</label>
			<div class="controls">
				<form:textarea path="bonusItems" htmlEscape="false" maxlength="200" rows="2"
					class="input-xlarge required" style="width:760px;resize:none" />
					<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">减分：</label>
			<div class="controls">
				<form:input path="deduction" type="text" maxlength="64"
					class="input-xlarge required" style="width:100px;" id="text2" onkeyup="jisuan()" />
					<span class="help-inline" id="resultse" color="red"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		<td>
		<div class="control-group">
			<label class="control-label">减分项目：</label>
			<div class="controls">
				<form:textarea path="minusItems" htmlEscape="false" maxlength="200" rows="2"
					class="input-xlarge required" style="width:760px;resize:none" />
					<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class="control-group">
			<label class="control-label">总分</label>
			<div class="controls">
				<form:input path="totalPoints" type="text" maxlength="64"
					class="input-xlarge required" style="width:100px;" id="text3" onkeyup="if(!/^\d+$/.test(this.value)) tip.innerHTML='必须输入数字，且不能有空格。'; else tip.innerHTML='';" />
					<span class="help-inline" id="tip"><font color="red">*</font> </span>
			</div>
		</div>
		</td>
		</tr>
		</table>
		<div class="control-group">
			<label class="control-label">备注：</label>
			<div class="controls">
				<form:textarea path="appraiseRemarks" htmlEscape="false" rows="4"
					maxlength="500" class="input-xlarge required" style="width:1070px;resize:none" />
					<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="checks:atcAppraise:edit">
				<input id="btnSubmit" class="btn btn-primary" type="submit"
					value="保 存" />&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回"
				onclick="history.go(-1)" />
		</div>
	</form:form>
</body>
</html>