<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<meta name="decorator" content="default"/>
	<link type="text/css" rel="stylesheet" href="${ctxStatic}/css/atc/atcCheckPrintView.css" />
	<script type="text/javascript">
			function printdiv(printpage) 
			{ 
			var headstr = "<html><head><title></title></head><body>"; 
			var footstr = "</body>"; 
			var newstr = document.all.item(printpage).innerHTML; 
			var oldstr = document.body.innerHTML; 
			document.body.innerHTML = headstr+newstr+footstr; 
			window.print(); 
			document.body.innerHTML = oldstr; 
			return false; 0
	} 
	
	</script>
	
	<style type="text/css">
	.btn-primary{
		margin-left: 300px;
		margin-top: 50px;
	}
	</style>
</head>
<body>
	  

 
	<div id="div_print"> 
 
	
	
	<div><sys:message content="${message}"/></div>
		<div class="div_02">
			<table>
				<caption>
					<div class="div_span_01"><span>体检表</span></div>
				</caption>
				<tr>
					<td class="td01">人员编号</td>
					<td class="td02" colspan="6">${atcCheck.no}</td>
				</tr>
				<tr class="tr_">
					<td class="td01" rowspan="2">体表特征</td>
					<td class="td03">体重</td>
					<td class="td13">${atcCheck.checkWeight}</td>
					<td class="td03">身高</td>
					<td class="td13">${atcCheck.checkHeight}</td>
					<td class="td03">血型</td>
					<td class="td13">${atcCheck.checkBloodType}</td>
				</tr>
				<tr class="tr_">
					<td class="td03">足长</td>
					<td class="td13">${atcCheck.checkLongEnough}</td>
					<td class="td03">有无残疾</td>
					<td class="td04" colspan="3">${atcCheck.checkDisabled}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">曾患何种疾病</td>
					<td class="td05" colspan="6">${atcCheck.checkDisease}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">曾做过何种手术或外伤史</td>
					<td class="td05" colspan="6">${atcCheck.checkHistoryofsurgery}</td>
				</tr>
				<tr class="tr_">
					<td class="td06" rowspan="5">外科</td>
					<td class="td07">腹部</td>
					<td class="td08" colspan="2">${atcCheck.checkAbdomen}</td>
					<td class="td07">腹部异常备注</td>
					<td class="td08" colspan="2">${atcCheck.checkAbdomenRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td09">脊柱</td>
					<td class="td08" colspan="2">${atcCheck.checkSpine}</td>
					<td class="td09">脊柱异常备注</td>
					<td class="td08" colspan="2">${atcCheck.checkSpineRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td07">皮肤</td>
					<td class="td08" colspan="2">${atcCheck.checkSkin}</td>
					<td class="td07">皮肤异常备注</td>
					<td class="td08" colspan="2">${atcCheck.checkSkinRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td09">四肢关节</td>
					<td class="td08" colspan="2">${atcCheck.checkLimbs}</td>
					<td class="td09">四肢关节异常备注</td>
					<td class="td08" colspan="2">${atcCheck.checkLimbsRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td07">体表损伤</td>
					<td class="td08" colspan="2">${atcCheck.checkBodyDamage}</td>
					<td class="td07">体表损伤异常备注</td>
					<td class="td08" colspan="2">${atcCheck.checkBodyDamageRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td06" rowspan="6">内科</td>
					<td class="td07">血压</td>
					<td class="td08" colspan="2">${atcCheck.checkBloodPressure}</td>
					<td class="td07">血压异常备注</td>
					<td class="td08" colspan="2">${atcCheck.checkBloodPressureRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td09">肺部</td>
					<td class="td08" colspan="2">${atcCheck.checkLungs}</td>
					<td class="td09">肺部异常备注</td>
					<td class="td08" colspan="2">${atcCheck.checkLungsRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">尿常规</td>
					<td class="td02" colspan="2">${atcCheck.checkUrineRoutine}</td>
					<td class="td01">尿常规异常备注</td>
					<td class="td02" colspan="2">${atcCheck.checkUrineRoutineRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">心脏</td>
					<td class="td02" colspan="2">${atcCheck.checkHeart}</td>
					<td class="td01">心脏异常备注</td>
					<td class="td02" colspan="2">${atcCheck.checkHeartRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td07">血常规</td>
					<td class="td08" colspan="2">${atcCheck.checkBloodRoutine}</td>
					<td class="td07">血常规异常备注</td>
					<td class="td08" colspan="2">${atcCheck.checkBloodRoutineRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td09">肝功情况</td>
					<td class="td08" colspan="2">${atcCheck.checkLiverFunction}</td>
					<td class="td09">肝功情况异常备注</td>
					<td class="td08" colspan="2">${atcCheck.checkLiverFunctionRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td06" rowspan="3">其它</td>
					<td class="td09">艾滋病情况</td>
					<td class="td08" colspan="5">${atcCheck.checkAidsSituation}</td>
				</tr>
				<tr class="tr_">
					<td class="td07">性病情况</td>
					<td class="td08" colspan="2">${atcCheck.checkSexuallyTransmittedDiseases}</td>
					<td class="td07">性病情况异常备注</td>
					<td class="td08" colspan="2">${atcCheck.checkSexuallyTransmittedDiseasesRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td07">急性病情况</td>
					<td class="td02" colspan="2">${atcCheck.checkAcuteDisease}</td>
					<td class="td07">急性病情况异常备注</td>
					<td class="td02" colspan="3">${atcCheck.checkAcuteDiseaseRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">心电图</td>
					<td class="td02">${atcCheck.checkElectrocardiogram}</td>
					<td class="td01">心电图异常备注</td>
					<td class="td02" colspan="4">${atcCheck.checkElectrocardiogramRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">胸透</td>
					<td class="td02">${atcCheck.checkThoracotomy}</td>
					<td class="td01">胸透异常备注</td>
					<td class="td02" colspan="4">${atcCheck.checkThoracotomyRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td01">B超</td>
					<td class="td02">${atcCheck.checkBsuper}</td>
					<td class="td01">B超异常备注</td>
					<td class="td02" colspan="4">${atcCheck.checkBsuperRemark}</td>
				</tr>
				<tr class="tr_">
					<td class="td10" rowspan="2">医生建议</td>
					<td class="td11" colspan="6">${atcCheck.checkDoctorAdvises}</td>
				</tr>
				<tr class="tr_">
					<td class="td12" colspan="6"> 签字：            日期：    年  月  日</td>
				</tr>
				<tr class="tr_">
					<td class="td10" rowspan="2">领导建议</td>
					<td class="td11" colspan="6">${atcCheck.checkLeadershipAdvice}</td>
				</tr>
				<tr class="tr_">
					<td class="td12" colspan="6"> 签字：            日期：    年  月  日</td>
				</tr>
			</table>
		</div>


	
	
 
</div> 
	<input name="b_print" type="button" class="btn btn-primary" onClick="printdiv('div_print');" value=" 打印 " > 
</body>
</html>