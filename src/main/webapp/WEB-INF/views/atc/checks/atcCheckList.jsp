<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>体检表管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/checks/atcCheck/">体检表列表</a></li>
		<shiro:hasPermission name="checks:atcCheck:edit"><li><a href="${ctx}/checks/atcCheck/form">体检表添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="atcCheck" action="${ctx}/checks/atcCheck/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>人员编号：</label>
				<form:input path="no" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>检查日期：</label>
				<input name="checkDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${atcCheck.checkDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>人员编号</th>
				<th>姓名</th>
				<th>体重</th>
				<th>身高</th>
				<th>血型</th>
				<th>足长</th>
				<th>血压</th>
				<th>肺部</th>
				<th>检查日期</th>
				<shiro:hasPermission name="checks:atcCheck:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="atcCheck">
			<tr>
				<td>
					${atcCheck.no}
				</td>
				<td>
					${atcCheck.name}
				</td>
				<td>
					${atcCheck.checkWeight}
				</td>
				<td>
					${atcCheck.checkHeight}
				</td>
				<td>
					${atcCheck.checkBloodType}
				</td>
				<td>
					${atcCheck.checkLongEnough}
				</td>
				<td>
					${atcCheck.checkBloodPressure}
				</td>
				<td>
					${atcCheck.checkLungs}
				</td>
				<td>
					<fmt:formatDate value="${atcCheck.checkDate}" pattern="yyyy-MM-dd"/>
				</td>
				<shiro:hasPermission name="checks:atcCheck:edit"><td>
    				<a href="${ctx}/checks/atcCheck/model?id=${atcCheck.id}">修改</a>
					<a href="${ctx}/checks/atcCheck/delete?id=${atcCheck.id}" onclick="return confirmx('确认要删除该体检表吗？', this.href)">删除</a>
						<a href="${ctx}/checks/atcCheck/printer?id=${atcCheck.id}">打印预览</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>