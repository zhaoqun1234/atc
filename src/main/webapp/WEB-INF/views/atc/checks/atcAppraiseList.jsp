<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>评分标准管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/checks/atcAppraise/">评分标准列表</a></li>
		<shiro:hasPermission name="checks:atcAppraise:edit"><li><a href="${ctx}/checks/atcAppraise/form">评分标准添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="atcAppraise" action="${ctx}/checks/atcAppraise/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>评分日期：</label>
				<input name="appraiseDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${atcAppraise.appraiseDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</li>
			<li><label>人员编号：</label>
				<form:input path="no" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
			
				<th>人员编号</th>
				<th>评分日期</th>
				<th>姓名</th>
				<th>加分</th>
				<th>减分</th>
				<th>总分</th>
				<th>备注</th>
				<th>加分项目</th>
				<th>减分项目</th>
				<shiro:hasPermission name="checks:atcAppraise:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="atcAppraise">
			<tr>
				<td>
					${atcAppraise.no}
				</td>
				<td>
					<fmt:formatDate value="${atcAppraise.appraiseDate}" pattern="yyyy-MM-dd"/>
				</td>
				
				<td>
					${atcAppraise.name}
				</td>
				<td>
					${atcAppraise.bonusPoint}
				</td>
				
				<td>
					${atcAppraise.deduction}
				</td>
				<td>
					${atcAppraise.totalPoints}
				</td>
				<td>
					${atcAppraise.appraiseRemarks}
				</td>
				<td>
					${atcAppraise.bonusItems}
				</td>
				<td>
					${atcAppraise.minusItems}
				</td>
				<shiro:hasPermission name="checks:atcAppraise:edit"><td>
    				<a href="${ctx}/checks/atcAppraise/model?id=${atcAppraise.id}">修改</a>
					<a href="${ctx}/checks/atcAppraise/delete?id=${atcAppraise.id}" onclick="return confirmx('确认要删除该评分标准吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>