/*人员健康表 验证*/
$(document).ready(function() {
	//$("#name").focus();
	$("#inputForm").validate({
		submitHandler: function(form){
			var tmp=$("#telephone").val();
			/* 这里是校验电话号码 */
		 	if(""!=tmp){
		 		if(!(/^1[3|4|5|7|8]\d{9}$/.test(tele))){
		 			$("#msgtelephone").text("电话的格式不正确");
		 			clearMsg($("#msgtelephone"));
		 			return false;
		 		}else{
					 $("#msgtelephone").text("*");
				 }
		 	}
			/*
		 	姓名 
			if(typeof(tmp)=="undefined" || ""==tmp){ 
				$("#msgname").text("请选择姓名");
				clearMsg($("#msgname"));
				return false;
			}else $("#msgname").text("*");
		 	开始时间
			tmp = $("#start").val();
			if(typeof(tmp)=="undefined" || ""==tmp){ 
				$("#msgstart").text("请选择开始时间");
				clearMsg($("#msgstart"));
				return false;
			}else $("#msgstart").text("*");
			结束时间
			tmp = $("#end").val();
			if(typeof(tmp)=="undefined" || ""==tmp){ 
				$("#msgend").text("请选择结束时间");
				clearMsg($("#msgend"));
				return false;
			}else $("#msgend").text("*");
			
			
			 ajax的部分 
			  var returnVal = true;
				var id = $("#sysUserIdId").val();
				$.ajax({
					url:ctx+"/ready/hcmUserBase/nameFind",
					type:"post",
					data:"id="+id,
					async:false,
					success:function(data){
						if(data=="true"){
							$("#nameId").text("用户已添加该资料")
							returnVal = false;
						}else{
							$("#nameId").text("*")
						}
					}
				});
				if(returnVal==false){
					return false;
				}*/
			
			
			loading('正在提交，请稍等...');
			form.submit();
		},
		errorContainer: "#messageBox",
		errorPlacement: function(error, element) {
			$("#messageBox").text("输入有误，请先更正。");
			if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
					error.appendTo(element.parent().parent());
				} else {
					error.insertAfter(element);
				}
			}
		});
});

/*打印预览*/
function printView(){
	var url=$("#ctx_01").val()+"/atcbase/atcBase/printView?id="+$("#id_01").val();
//	弹层展示
	layer.open({
		type: 2,
		title: '人员健康检查表打印预览。',
		shadeClose: true,
		shade: 0.6,
		maxmin: true, //开启最大化最小化按钮
		area: ['1265px', '800px'],
		content: url
	});
}

//清除提示
function clearMsg(obj){
	setTimeout(function(){
		obj.html("*");
	},2000);
}