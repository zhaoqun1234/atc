/*人员健康表 验证*/
$(document).ready(function() {
	var oldId = $("#fkIdId").val();
	//$("#name").focus();
	$("#inputForm").validate({
		submitHandler: function(form){
			var tmp=$("#fkIdName").val();
		 	/*姓名 */
			if(typeof(tmp)=="undefined" || ""==tmp){ 
				$("#msgfkId").text("请选择姓名");
				clearMsg($("#msgfkId"));
				return false;
			}else $("#msgfkId").text("*");
		 	/*开始时间*/
			tmp = $("#start").val();
			if(typeof(tmp)=="undefined" || ""==tmp){ 
				$("#msgstart").text("请选择开始时间");
				clearMsg($("#msgstart"));
				return false;
			}else $("#msgstart").text("*");
			/*结束时间*/
			tmp = $("#end").val();
			if(typeof(tmp)=="undefined" || ""==tmp){ 
				$("#msgend").text("请选择结束时间");
				clearMsg($("#msgend"));
				return false;
			}else $("#msgend").text("*");
			
			var id = $("#fkIdId").val();
			
			var returnVal = true;
			if(oldId != id){
				$.ajax({
					url:ctx+"/peoplehealth/atcHealth/findRole",
					type:"post",
					data:"fkId="+id,
					async:false,
					success:function(data){
						if(data=="true"){
							$("#msgfkId").text("该人员资料已添加");
							clearMsg($("#msgfkId"));
							returnVal = false;
						}else{
							$("#msgfkId").text("*");
							returnVal = true;
						}
					}
				});
			}
			if(returnVal==false){
				return false;
			}
			
			loading('正在提交，请稍等...');
			form.submit();
		},
		errorContainer: "#messageBox",
		errorPlacement: function(error, element) {
			$("#messageBox").text("输入有误，请先更正。");
			if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
					error.appendTo(element.parent().parent());
				} else {
					error.insertAfter(element);
				}
			}
		});
	
});

//验证该人员是否已经录入
function validateFiId(){
	var id = $("#fkIdId").val();
	$.ajax({
		url:ctx+"/peoplehealth/atcHealth/findRole",
		type:"post",
		data:"fkId="+id,
		async:false,
		success:function(data){
			if(data=="true"){
				$("#msgfkId").text("该人员资料已添加");
				clearMsg($("#msgfkId"));
				return false;
			}else{
				$("#msgfkId").text("*");
				return true;
			}
		}
	});
}

/*打印预览*/
function printView(){
	var url=$("#ctx_01").val()+"/peoplehealth/atcHealth/printView?id="+$("#id_01").val();
	
//	弹层展示
	layer.open({
		type: 2,
		title: '人员健康检查表打印预览。',
		shadeClose: true,
		shade: 0.6,
		maxmin: true, //开启最大化最小化按钮
		area: ['1265px', '800px'],
		content: url
	});
	
}

//清除提示
function clearMsg(obj){
	setTimeout(function(){
		obj.html("*");
	},2000);
}