/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.laws.dao;

import java.util.List;  

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;

import com.thinkgem.jeesite.modules.atc.laws.entity.AtcLaws;

/**
 * 法律手续表DAO接口
 * @author 孟朋朋
 * @version 2017-03-15
 */
@MyBatisDao
public interface AtcLawsDao extends CrudDao<AtcLaws> {
	/**
	 * 查找所有的角色
	 * @param tableName
	 * @return
	 */
	
	public List<AtcLaws> findRole(String tableName);
}