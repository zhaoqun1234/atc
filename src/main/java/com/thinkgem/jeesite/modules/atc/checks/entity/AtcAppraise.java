/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.checks.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 评分标准表Entity
 * @author 孙元智
 */
public class AtcAppraise extends DataEntity<AtcAppraise> {
	
	private static final long serialVersionUID = 1L;
	/*考核日期  */
	private Date appraiseDate;		 
	/*检查编号  */
	private String appraiseId;		 
	/*加分      */
	private String bonusPoint;		
	/*减分	   */
	private String deduction;		 
	/*总分	   */
	private String totalPoints;		 
	/*备注	   */
	private String appraiseRemarks;		 
	/*加分项目 */
	private String bonusItems;		 
	/*减分项目 */
	private String minusItems;		 
	/*人员编号 */
	private String no;              
	/*姓名	   */
	private String name;           
	
	
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public AtcAppraise() {
		super();
	}

	public AtcAppraise(String id){
		super(id);
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getAppraiseDate() {
		return appraiseDate;
	}

	public void setAppraiseDate(Date appraiseDate) {
		this.appraiseDate = appraiseDate;
	}
	
	@Length(min=0, max=64, message="人员编号长度必须介于 0 和 64 之间")
	public String getAppraiseId() {
		return appraiseId;
	}

	public void setAppraiseId(String appraiseId) {
		this.appraiseId = appraiseId;
	}
	
	@Length(min=0, max=64, message="加分长度必须介于 0 和 64 之间")
	public String getBonusPoint() {
		return bonusPoint;
	}

	public void setBonusPoint(String bonusPoint) {
		this.bonusPoint = bonusPoint;
	}
	
	@Length(min=0, max=64, message="减分长度必须介于 0 和 64 之间")
	public String getDeduction() {
		return deduction;
	}

	public void setDeduction(String deduction) {
		this.deduction = deduction;
	}
	
	@Length(min=0, max=64, message="总分长度必须介于 0 和 64 之间")
	public String getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(String totalPoints) {
		this.totalPoints = totalPoints;
	}
	
	@Length(min=0, max=500, message="备注长度必须介于 0 和 500 之间")
	public String getAppraiseRemarks() {
		return appraiseRemarks;
	}

	public void setAppraiseRemarks(String appraiseRemarks) {
		this.appraiseRemarks = appraiseRemarks;
	}
	
	@Length(min=0, max=500, message="加分项目长度必须介于 0 和 500 之间")
	public String getBonusItems() {
		return bonusItems;
	}

	public void setBonusItems(String bonusItems) {
		this.bonusItems = bonusItems;
	}
	
	@Length(min=0, max=500, message="减分项目长度必须介于 0 和 500 之间")
	public String getMinusItems() {
		return minusItems;
	}

	public void setMinusItems(String minusItems) {
		this.minusItems = minusItems;
	}
	
}