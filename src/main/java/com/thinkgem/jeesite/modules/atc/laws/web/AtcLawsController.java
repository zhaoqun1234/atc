/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.laws.web;

import java.io.IOException; 
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;

import com.thinkgem.jeesite.modules.atc.laws.entity.AtcLaws;
import com.thinkgem.jeesite.modules.atc.laws.service.AtcLawsService;

/**
 * 法律手续表Controller
 * @author 孟朋朋
 * @version 2017-03-15
 */
@Controller
@RequestMapping(value = "${adminPath}/laws/atcLaws")
public class AtcLawsController extends BaseController {

	@Autowired
	private AtcLawsService atcLawsService;
	
	@ModelAttribute
	public AtcLaws get(@RequestParam(required=false) String id) {
		AtcLaws entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = atcLawsService.get(id);
		}
		if (entity == null){
			entity = new AtcLaws();
		}
		return entity;
	}
	@RequiresPermissions("laws:atcLaws:view")
	@RequestMapping(value = "model")
	public String model(AtcLaws atcLaws, Model model) {
		model.addAttribute("atcLaws", atcLaws);
		return "atc/laws/atcLawsModel";
	}
	
	@RequiresPermissions("laws:atcLaws:view")
	@RequestMapping(value = {"list", ""})
	public String list(AtcLaws atcLaws, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcLaws> page = atcLawsService.findPage(new Page<AtcLaws>(request, response), atcLaws); 
		model.addAttribute("page", page);
		return "atc/laws/atcLawsList";
	}

	@RequiresPermissions("laws:atcLaws:view")
	@RequestMapping(value = "form")
	public String form(AtcLaws atcLaws, Model model) {
		model.addAttribute("atcLaws", atcLaws);
		return "atc/laws/atcLawsForm";
	}

	@RequiresPermissions("laws:atcLaws:edit")
	@RequestMapping(value = "save")
	public String save(AtcLaws atcLaws, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, atcLaws)){
			return form(atcLaws, model);
		}
		atcLawsService.save(atcLaws);
		addMessage(redirectAttributes, "保存法律手续表成功");
		return "redirect:"+Global.getAdminPath()+"/laws/atcLaws/?repage";
	}
	
	@RequiresPermissions("laws:atcLaws:edit")
	@RequestMapping(value = "delete")
	public String delete(AtcLaws atcLaws, RedirectAttributes redirectAttributes) {
		atcLawsService.delete(atcLaws);
		addMessage(redirectAttributes, "删除法律手续表成功");
		return "redirect:"+Global.getAdminPath()+"/laws/atcLaws/?repage";
	}
	/**
	 * 
	 * printer:这里是将数据传到模板里并且按一定模板，生成法律手续
	 * 
	 * @param atcLaws：法律手续信息实体类对象
	 * @param model：把数据从controller传递到前端的载体
	 * @return：返回到一个提示页面，提示生成word文档是否成功
	 */
	@RequiresPermissions("laws:atcLaws:view")
	@RequestMapping(value = "printer")
	public String printer(AtcLaws atcLaws, Model model) {
		model.addAttribute("atcLaws", atcLaws);
		return "atc/laws/atcLawPrintView";
	}
	/**
	 * findRole:这里是验证用户是否重复添加，通过获取用户的姓名获取其tableName去数据库验证
	 * @param response响应页面
	 * @param tableName:姓名所对应id
	 * @param redirectAttributes:
	 */
	@RequiresPermissions("laws:atcLaws:view")
	@RequestMapping(value = "findRole")
	public void findRole(HttpServletResponse response ,String tableName,RedirectAttributes redirectAttributes){
		List<AtcLaws> atcLaws=atcLawsService.findRole(tableName);
		response.setContentType("application/text;charset=utf-8");
		
		try {
			if(atcLaws.size()>0){
				response.getWriter().print("true");
			}else{
				response.getWriter().print("false");
			}
		} catch (IOException e) {
			addMessage(redirectAttributes, "用户校验失败！失败信息："+e.getMessage());
		}
	}
}