/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.peoplehealth.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.atc.peoplehealth.entity.AtcHealth;

/**
 * 人员健康检查表DAO接口
 * @author 曹红
 * @version 2017-03-13
 */
@MyBatisDao
public interface AtcHealthDao extends CrudDao<AtcHealth> {
	
	/**
	 * 根据人员健康检查表的外键查询相关人员的人数,
	 * 以此来判断该人员的健康检查信息是否录入
	 * @param fkId 人员健康检查表的外键
	 * @return 查询到的人数   ==0：该人员没有录入，>0,该人员已经录入
	 */
	public int findRole(String fkId);
	
}