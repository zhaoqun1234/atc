 /****************************************************/
 /* Project Name:戒毒信息管理                             */
 /* File Name:AtcSocietyRelationshipController             */
 /* Date:2017年2月21日下午5:59:12            */
 /* Description:社会人员关系向关的业务逻辑方法      */
 /****************************************************/ 
package com.thinkgem.jeesite.modules.atc.atcbase.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.atc.atcbase.entity.AtcSocietyRelationship;
import com.thinkgem.jeesite.modules.atc.atcbase.service.AtcSocietyRelationshipService;

/**
 * 
 * @author zq
 *
 */
@Controller
@RequestMapping(value = "${adminPath}/atcbase/atcSocietyRelationship")
public class AtcSocietyRelationshipController extends BaseController {

	@Autowired
	private AtcSocietyRelationshipService atcSocietyRelationshipService;
	
	@ModelAttribute
	public AtcSocietyRelationship get(@RequestParam(required=false) String id) {
		AtcSocietyRelationship entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = atcSocietyRelationshipService.get(id);
		}
		if (entity == null){
			entity = new AtcSocietyRelationship();
		}
		return entity;
	}
	
	@RequiresPermissions("atcbase:atcSocietyRelationship:view")
	@RequestMapping(value = {"list", ""})
	public String list(AtcSocietyRelationship atcSocietyRelationship, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcSocietyRelationship> page = atcSocietyRelationshipService.findPage(new Page<AtcSocietyRelationship>(request, response), atcSocietyRelationship); 
		model.addAttribute("page", page);
		return "atc/atcbase/atcSocietyRelationshipList";
	}

	@RequiresPermissions("atcbase:atcSocietyRelationship:view")
	@RequestMapping(value = "form")
	public String form(AtcSocietyRelationship atcSocietyRelationship, Model model) {
		model.addAttribute("atcSocietyRelationship", atcSocietyRelationship);
		return "atc/atcbase/atcSocietyRelationshipForm";
	}

	@RequiresPermissions("atcbase:atcSocietyRelationship:edit")
	@RequestMapping(value = "save")
	public String save(AtcSocietyRelationship atcSocietyRelationship, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, atcSocietyRelationship)){
			return form(atcSocietyRelationship, model);
		}
		atcSocietyRelationshipService.save(atcSocietyRelationship);
		addMessage(redirectAttributes, "保存保存基本信息成功成功");
		return "redirect:"+Global.getAdminPath()+"/atcbase/atcSocietyRelationship/?repage";
	}
	
	@RequiresPermissions("atcbase:atcSocietyRelationship:edit")
	@RequestMapping(value = "delete")
	public String delete(AtcSocietyRelationship atcSocietyRelationship, RedirectAttributes redirectAttributes) {
		atcSocietyRelationshipService.delete(atcSocietyRelationship);
		addMessage(redirectAttributes, "删除保存基本信息成功成功");
		return "redirect:"+Global.getAdminPath()+"/atcbase/atcSocietyRelationship/?repage";
	}

}