/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.checks.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.atc.checks.entity.AtcAppraise;

/**
 * 评分标准表DAO接口
 * @author 孙元智
 * @version 2017-03-13
 */
@MyBatisDao
public interface AtcAppraiseDao extends CrudDao<AtcAppraise> {
	
}