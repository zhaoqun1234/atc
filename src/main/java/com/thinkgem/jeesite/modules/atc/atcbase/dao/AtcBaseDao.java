/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.atcbase.dao;

import java.util.List;
import java.util.Map;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.atc.atcbase.entity.AtcBase;

/**
 * 基本信息DAO接口
 * @author zq
 * @version 2017-03-08
 */
@MyBatisDao
public interface AtcBaseDao extends CrudDao<AtcBase> {
	/*杨雪超接口开始*/
	
	
	/**
	 * 
	 * sex:根据性别
	 * 
	 */
	public List<Map<String, Object>> sex();
	
	/**
	 * 
	 * education:根据受教育程度
	 * 
	 */
	
	public List<Map<String, Object>> education();
	
	
	/**
	 * 
	 * national:按照民族统计计算民族最多的前三个民族的人返回键值对,比如说A,民族有十个人的list
	 * 
	 */
	public List<Map<String, Object>> national();
	
	/**
	 * 
	 * underTwenty:查看20岁以下有多少人
	 * 
	 */
	public int underTwenty();
	/**
	 * 
	 * twentyFive:查看20-25岁之间有多少人
	 * 
	 */
	public int twentyFive();
	/**
	 * 
	 * thirty:查看25-30之间的人数
	 * 
	 */
	public int thirty();
	/**
	 * 
	 * thirtyFive:30-35之间的人数
	 * 
	 */
	public int thirtyFive();
	/**
	 * 
	 * moreThirty:查看35以上的人数
	 * 
	 */
	public int moreThirty();

	public AtcBase No();

	public int count();


	
	/*杨雪超接口结束*/
	
}