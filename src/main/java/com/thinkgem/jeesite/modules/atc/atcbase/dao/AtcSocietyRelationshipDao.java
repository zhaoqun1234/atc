/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.atcbase.dao;

import java.util.List;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.atc.atcbase.entity.AtcSocietyRelationship;

/**
 * 基本信息DAO接口
 * @author zq
 * @version 2017-03-08
 */
@MyBatisDao
public interface AtcSocietyRelationshipDao extends CrudDao<AtcSocietyRelationship> {

	public  List<AtcSocietyRelationship> findRelationshipList(String id);

	public int delete_relationship(String id);

	public int updateMessage(AtcSocietyRelationship societyRelationship);
	
}