/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.peoplehealth.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 人员健康检查表Entity
 * @author 曹红
 * @version 2017-03-13
 */
public class AtcHealth extends DataEntity<AtcHealth> {
	
	private static final long serialVersionUID = 1L;
	
	/* 人员编号  */
	private String peopleNo;
	/* 姓名   */
	private String name;		
	/* 外键   */
	private String fkId;		
	/* 病房号   */
	private String room;		
	/* 病床号   */
	private String bed;		
	/* 住院号   */
	private String nu;		
	/* 病史   */
	private String history;		
	/* 确诊   */
	private String sure;		
	/* 确诊情况   */
	private String sureState;		
	/* 特殊病情   */
	private String specialCondition;		
	/* 自残情况   */
	private String autotomy;		
	/* 其他情况   */
	private String other;		
	/* 开始时间（医嘱）   */
	private Date start;
	/* 开始时医嘱   */
	private String startAdvice;		
	/* 开始时医生   */
	private String startDoctor;		
	/* 开始时护士   */
	private String startNurse;		
	/* 结束时间   */
	private Date end;		
	/* 结束时医嘱   */
	private String endAdvice;		
	/* 结束时医生   */
	private String endDoctor;		
	/* 结束时护士   */
	private String endNurse;		
	/* 脉搏   */
	private String pulse;		
	/* 体温   */
	private String thermometer;		
	/* 呼吸   */
	private String breathe;		
	/* 大便次数   */
	private String shitTimes;		
	/* 血压   */
	private String bloodPressure;		
	/* 总入量   */
	private String totalInput;		
	/* 总出量   */
	private String totalOutput;		
	/* 尿量   */
	private String urineVolume;		
	/* 体重   */
	private String weight;		
	
	public String getPeopleNo() {
		return peopleNo;
	}

	public void setPeopleNo(String peopleNo) {
		this.peopleNo = peopleNo;
	}

	public AtcHealth() {
		super();
	}

	public AtcHealth(String id){
		super(id);
	}

	@Length(min=1, max=10, message="姓名长度必须介于 1 和 10 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=200, message="外键长度必须介于 0 和 200 之间")
	public String getFkId() {
		return fkId;
	}

	public void setFkId(String fkId) {
		this.fkId = fkId;
	}
	
	@Length(min=0, max=200, message="病房号长度必须介于 0 和 200 之间")
	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}
	
	@Length(min=0, max=10, message="病床号长度必须介于 0 和 10 之间")
	public String getBed() {
		return bed;
	}

	public void setBed(String bed) {
		this.bed = bed;
	}
	
	@Length(min=0, max=200, message="住院号长度必须介于 0 和 200 之间")
	public String getNu() {
		return nu;
	}

	public void setNu(String nu) {
		this.nu = nu;
	}
	
	@Length(min=0, max=200, message="病史长度必须介于 0 和 200 之间")
	public String getHistory() {
		return history;
	}

	public void setHistory(String history) {
		this.history = history;
	}
	
	@Length(min=0, max=200, message="确诊长度必须介于 0 和 200 之间")
	public String getSure() {
		return sure;
	}

	public void setSure(String sure) {
		this.sure = sure;
	}
	
	@Length(min=0, max=200, message="确诊情况长度必须介于 0 和 200 之间")
	public String getSureState() {
		return sureState;
	}

	public void setSureState(String sureState) {
		this.sureState = sureState;
	}
	
	@Length(min=0, max=200, message="特殊病情长度必须介于 0 和 200 之间")
	public String getSpecialCondition() {
		return specialCondition;
	}

	public void setSpecialCondition(String specialCondition) {
		this.specialCondition = specialCondition;
	}
	
	@Length(min=0, max=200, message="自残情况长度必须介于 0 和 200 之间")
	public String getAutotomy() {
		return autotomy;
	}

	public void setAutotomy(String autotomy) {
		this.autotomy = autotomy;
	}
	
	@Length(min=0, max=200, message="其他情况长度必须介于 0 和 200 之间")
	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}
	
	@Length(min=0, max=200, message="开始时医嘱长度必须介于 0 和 200 之间")
	public String getStartAdvice() {
		return startAdvice;
	}

	public void setStartAdvice(String startAdvice) {
		this.startAdvice = startAdvice;
	}
	
	@Length(min=0, max=10, message="开始时医生长度必须介于 0 和 10 之间")
	public String getStartDoctor() {
		return startDoctor;
	}

	public void setStartDoctor(String startDoctor) {
		this.startDoctor = startDoctor;
	}
	
	@Length(min=0, max=10, message="开始时护士长度必须介于 0 和 10 之间")
	public String getStartNurse() {
		return startNurse;
	}

	public void setStartNurse(String startNurse) {
		this.startNurse = startNurse;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
	
	@Length(min=0, max=200, message="结束时医嘱长度必须介于 0 和 200 之间")
	public String getEndAdvice() {
		return endAdvice;
	}

	public void setEndAdvice(String endAdvice) {
		this.endAdvice = endAdvice;
	}
	
	@Length(min=0, max=10, message="结束时医生长度必须介于 0 和 10 之间")
	public String getEndDoctor() {
		return endDoctor;
	}

	public void setEndDoctor(String endDoctor) {
		this.endDoctor = endDoctor;
	}
	
	@Length(min=0, max=10, message="结束时护士长度必须介于 0 和 10 之间")
	public String getEndNurse() {
		return endNurse;
	}

	public void setEndNurse(String endNurse) {
		this.endNurse = endNurse;
	}
	
	@Length(min=0, max=3, message="脉搏长度必须介于 0 和 3 之间")
	public String getPulse() {
		return pulse;
	}

	public void setPulse(String pulse) {
		this.pulse = pulse;
	}
	
	@Length(min=0, max=2, message="体温长度必须介于 0 和 2 之间")
	public String getThermometer() {
		return thermometer;
	}

	public void setThermometer(String thermometer) {
		this.thermometer = thermometer;
	}
	
	@Length(min=0, max=2, message="呼吸长度必须介于 0 和 2 之间")
	public String getBreathe() {
		return breathe;
	}

	public void setBreathe(String breathe) {
		this.breathe = breathe;
	}
	
	@Length(min=0, max=2, message="大便次数长度必须介于 0 和 2 之间")
	public String getShitTimes() {
		return shitTimes;
	}

	public void setShitTimes(String shitTimes) {
		this.shitTimes = shitTimes;
	}
	
	@Length(min=0, max=3, message="血压长度必须介于 0 和 3 之间")
	public String getBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	
	@Length(min=0, max=200, message="总入量长度必须介于 0 和 200 之间")
	public String getTotalInput() {
		return totalInput;
	}

	public void setTotalInput(String totalInput) {
		this.totalInput = totalInput;
	}
	
	@Length(min=0, max=200, message="总出量长度必须介于 0 和 200 之间")
	public String getTotalOutput() {
		return totalOutput;
	}

	public void setTotalOutput(String totalOutput) {
		this.totalOutput = totalOutput;
	}
	
	@Length(min=0, max=200, message="尿量长度必须介于 0 和 4 之间")
	public String getUrineVolume() {
		return urineVolume;
	}

	public void setUrineVolume(String urineVolume) {
		this.urineVolume = urineVolume;
	}
	
	@Length(min=0, max=200, message="体重长度必须介于 0 和 3 之间")
	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}
	
}