 /****************************************************/
 /* Project Name:戒毒信息管理                             */
 /* File Name:AtBaseService              */
 /* Date:2017年2月21日下午5:59:12            */
 /* Description:基础信息的service的方法层       */
 /****************************************************/ 
package com.thinkgem.jeesite.modules.atc.atcbase.service;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.atc.atcbase.entity.AtcBase;



import com.thinkgem.jeesite.modules.atc.atcbase.dao.AtcBaseDao;

/**
 * 
 * @author zq
 *
 */
@Service
@Transactional(readOnly = true)
public class AtcBaseService extends CrudService<AtcBaseDao, AtcBase> {
	
	
	
	@Autowired
	private AtcBaseDao atcBaseDao;

	public AtcBase get(String id) {
		return super.get(id);
	}
	
	public List<AtcBase> findList(AtcBase atcBase) {
		return super.findList(atcBase);
	}
	
	public Page<AtcBase> findPage(Page<AtcBase> page, AtcBase atcBase) {
		return super.findPage(page, atcBase);
	}
	
	public Page<AtcBase> findPageTimes(Page<AtcBase> page, AtcBase atcBase) {
		return super.findPageTimes(page, atcBase);
	}
	
	@Transactional(readOnly = false)
	public void save(AtcBase atcBase) {
		super.save(atcBase);
	}
	
	@Transactional(readOnly = false)
	public void delete(AtcBase atcBase) {
		super.delete(atcBase);
	}
	/*得到最新的戒毒人员编号*/
	public AtcBase No(){
		return atcBaseDao.No();
	}
	public int count(){
		return atcBaseDao.count();
	}
	/**
	 * 
	 * findNo:人员编号的接口方法,获取自动生成的编号
	 * @return {@link String}<{@link time}>
	 */
	public String findNo() {
		 int number=1;
	     String aa="";
		 String bb="";
		String no="";
		String time2;
		Date create;
		int time22=0;
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String time1 = format.format(Calendar.getInstance().getTime());
		int time11=Integer.parseInt(time1);
		int count=count();
		if(count!=0){
			no=No().getNo();
		 create=No().getCreateDate();
		 time2=format.format(create);
		 time22=Integer.parseInt(time2);
		}
		  if(count==0){
			  number=1;
		  }else{
			  if(time11>time22){
				  number=1;
			  }else{
				  bb=no.substring(8);
				  number=Integer.parseInt(bb)+1;
			  }
		  }
		
		if(number<10){
			 aa="000"+number;
		}
		if(number>=10 && number<100){
			 aa="00"+number;
		}
		if(number>=100 && number<1000){
			 aa="0"+number;
		}
		String tie=time1+aa;
		
		return tie;
	}
	
	/*孙元智开始*/
	/**
	 * 
	 * findPageLists:对出所人员浏览页面进行分页
	 * 
	 * @param page:分页
	 * @param atcBase:戒毒人员基本信息实体类对象
	 * @return {@link Page}<{@link AtcBase}>
	 */
	public Page<AtcBase> findPageLists(Page<AtcBase> page, AtcBase atcBase) {
		return super.findPageList(page, atcBase);
	}
	
	/**
	 * 
	 * findPages:未来十天将要出所的人员信息进行分页
	 * 
	 * @param page:分页
	 * @param atcBase:戒毒人员基本信息实体类对象
	 * @return {@link Page}<{@link AtcBase}>
	 */
	public Page<AtcBase> findPages(Page<AtcBase> page, AtcBase atcBase) {
		return super.findPages(page, atcBase);
	}
	
	/**
	 * 
	 * findTwentyPage:未来二十天将要出所的人员信息进行分页
	 * 
	 * @param page:分页
	 * @param atcBase:戒毒人员基本信息实体类对象
	 * @return {@link Page}<{@link AtcBase}>
	 */
	public Page<AtcBase> findTwentyPage(Page<AtcBase> page, AtcBase atcBase) {
		return super.findTwentyDaysOutPerson(page, atcBase);
	}
	
	/**
	 * 
	 * findThirtyPage:未来三十天将要出所的人员信息进行分页
	 * 
	 * @param page:分页
	 * @param atcBase:戒毒人员基本信息实体类对象
	 * @return {@link Page}<{@link AtcBase}>
	 */
	public Page<AtcBase> findThirtyPage(Page<AtcBase> page, AtcBase atcBase) {
		return super.findThirtyDaysOutPerson(page, atcBase);
	}
	/*孙元智结束*/
	
	/*杨雪超service开始*/
	
	/**
	 * 
	 * sex:这里是不同性别人数的方法
	 * 
	 * @return 返回不同人数的教育方法
	 */
	public List<Map<String, Object>> sex(){
		return atcBaseDao.sex();
	}
	
	/**
	 * 
	 * national:查询民族人数的方法
	 * 
	 * @return 返回各个民族人数的前三名的键值对
	 */
	public List<Map<String, Object>> national(){
		return atcBaseDao.national();
	}
	
	/**
	 * education:查询不同受教育程度人数的方法
	 * 
	 * @return
	 */
	public List<Map<String, Object>> education(){
		return atcBaseDao.education();
	}
	
	
	/**
	 * 
	 * underTwenty:20岁以下的人数
	 * 
	 * @return 返回年龄在20岁以下的人数
	 */
	public int underTwenty(){
		return atcBaseDao.underTwenty();
	}
	/**
	 * 
	 * twentyFive:20-25人数的方法
	 * 
	 * @return 返回人数在20-25岁之间的人数
	 */
	public int twentyFive(){
	return atcBaseDao.twentyFive();
	}
	/**
	 * 
	 * thirty:30岁人员的方法
	 * 
	 * @return 返回人数在25-30岁之间的人数
	 */
	public int thirty(){
		return atcBaseDao.thirty();
	}
	/**
	 * 
	 * thirtyFive:35岁的人数的的方法
	 * 
	 * @return 返回年龄在30-35岁的人数
	 */
	public int thirtyFive(){
		return atcBaseDao.thirtyFive();
	}
	/**
	 * 
	 * moreThirty:35岁以上的方法
	 * 
	 * @return 返回人数在35岁以上的人数
	 */
	public int moreThirty(){
		return atcBaseDao.moreThirty();
	}

	/*杨雪超service结束*/
	
	
	
}