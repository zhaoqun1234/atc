/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.checks.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.atc.checks.entity.AtcCheck;
import com.thinkgem.jeesite.modules.atc.checks.service.AtcCheckService;

/**
 * 体检表Controller
 * @author 孙元智
 * @version 2017-03-15
 */
@Controller
@RequestMapping(value = "${adminPath}/checks/atcCheck")
public class AtcCheckController extends BaseController {

	@Autowired
	private AtcCheckService atcCheckService;
	
	@ModelAttribute
	public AtcCheck get(@RequestParam(required=false) String id) {
		AtcCheck entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = atcCheckService.get(id);
		}
		if (entity == null){
			entity = new AtcCheck();
		}
		return entity;
	}
	
	@RequiresPermissions("checks:atcCheck:view")
	@RequestMapping(value = {"list", ""})
	public String list(AtcCheck atcCheck, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcCheck> page = atcCheckService.findPage(new Page<AtcCheck>(request, response), atcCheck); 
		model.addAttribute("page", page);
		return "atc/checks/atcCheckList";
	}

	@RequiresPermissions("checks:atcCheck:view")
	@RequestMapping(value = "form")
	public String form(AtcCheck atcCheck, Model model) {
		model.addAttribute("atcCheck", atcCheck);
		return "atc/checks/atcCheckForm";
	}
	
	@RequiresPermissions("checks:atcCheck:view")
	@RequestMapping(value = "model")
	public String model(AtcCheck atcCheck, Model model) {
		model.addAttribute("atcCheck", atcCheck);
		return "atc/checks/updateInformation";
	}
	
	@RequiresPermissions("checks:atcCheck:edit")
	@RequestMapping(value = "save")
	public String save(AtcCheck atcCheck, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, atcCheck)){
			return form(atcCheck, model);
		}
		atcCheckService.save(atcCheck);
		addMessage(redirectAttributes, "保存体检表成功");
		return "redirect:"+Global.getAdminPath()+"/checks/atcCheck/?repage";
	}
	
	@RequiresPermissions("checks:atcCheck:edit")
	@RequestMapping(value = "delete")
	public String delete(AtcCheck atcCheck, RedirectAttributes redirectAttributes) {
		atcCheckService.delete(atcCheck);
		addMessage(redirectAttributes, "删除体检表成功");
		return "redirect:"+Global.getAdminPath()+"/checks/atcCheck/?repage";
	}
	
	@RequestMapping(value = "printer")
	public String printer(AtcCheck atcCheck, Model model) {
		model.addAttribute("atcCheck", atcCheck);
		return "atc/checks/atcCheckPrintView";
	}
	
}