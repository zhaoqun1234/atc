/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.checks.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.atc.checks.entity.AtcCheck;
import com.thinkgem.jeesite.modules.atc.checks.dao.AtcCheckDao;

/**
 * 体检表Service
 * @author 孙元智
 * @version 2017-03-15
 */
@Service
@Transactional(readOnly = true)
public class AtcCheckService extends CrudService<AtcCheckDao, AtcCheck> {

	public AtcCheck get(String id) {
		return super.get(id);
	}
	
	public List<AtcCheck> findList(AtcCheck atcCheck) {
		return super.findList(atcCheck);
	}
	
	public Page<AtcCheck> findPage(Page<AtcCheck> page, AtcCheck atcCheck) {
		return super.findPage(page, atcCheck);
	}
	
	@Transactional(readOnly = false)
	public void save(AtcCheck atcCheck) {
		super.save(atcCheck);
	}
	
	@Transactional(readOnly = false)
	public void delete(AtcCheck atcCheck) {
		super.delete(atcCheck);
	}
	
}