package com.thinkgem.jeesite.modules.atc.atcUtil;

public class Util {
	public static final String SAVE_SUCCESS = "保存成功";
	public static final String SAVE_FAILURE = "保存失败";
	public static final String DELETE_SUCCESS = "删除成功";
	public static final String DELETE_FAILURE = "删除失败";
	public static final String MODIFY_SUCCESS = "修改成功";
	public static final String MODIFY_FAILURE = "修改失败";
	public static final String CHECK_SUCCESS = "校验成功";
	public static final String CHECK_FALURE = "校验失败";
	public static final String APPLICATION_CODE = "application/text;charset=utf-8";
}
