 /****************************************************/
 /* Project Name:atc                                 */
 /* File Name:AtcAppraiseController.java             */
 /* Date:2017年3月13日下午5:59:12                      */
 /* Description  ：评分标准Controller                 	*/
 /***************************************************/  
package com.thinkgem.jeesite.modules.atc.checks.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.atc.checks.entity.AtcAppraise;
import com.thinkgem.jeesite.modules.atc.checks.entity.AtcCheck;
import com.thinkgem.jeesite.modules.atc.checks.service.AtcAppraiseService;

/**
 * 评分标准表Controller
 * @author 孙元智
 * @version 2017-03-13
 */
@Controller
@RequestMapping(value = "${adminPath}/checks/atcAppraise")
public class AtcAppraiseController extends BaseController {

	@Autowired
	private AtcAppraiseService atcAppraiseService;
	
	@ModelAttribute
	public AtcAppraise get(@RequestParam(required=false) String id) {
		AtcAppraise entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = atcAppraiseService.get(id);
		}
		if (entity == null){
			entity = new AtcAppraise();
		}
		return entity;
	}
	
	@RequiresPermissions("checks:atcAppraise:view")
	@RequestMapping(value = {"list", ""})
	public String list(AtcAppraise atcAppraise, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcAppraise> page = atcAppraiseService.findPage(new Page<AtcAppraise>(request, response), atcAppraise); 
		model.addAttribute("page", page);
		return "atc/checks/atcAppraiseList";
	}

	@RequiresPermissions("checks:atcAppraise:view")
	@RequestMapping(value = "form")
	public String form(AtcAppraise atcAppraise, Model model) {
		model.addAttribute("atcAppraise", atcAppraise);
		return "atc/checks/atcAppraiseForm";
	}
	
	@RequiresPermissions("checks:atcAppraise:view")
	@RequestMapping(value = "model")
	public String model(AtcAppraise atcAppraise, Model model) {
		model.addAttribute("atcAppraise", atcAppraise);
		return "atc/checks/updateInfo";
	}
	
	@RequiresPermissions("checks:atcAppraise:edit")
	@RequestMapping(value = "save")
	public String save(AtcAppraise atcAppraise, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, atcAppraise)){
			return form(atcAppraise, model);
		}
		atcAppraiseService.save(atcAppraise);
		addMessage(redirectAttributes, "保存评分标准成功");
		return "redirect:"+Global.getAdminPath()+"/checks/atcAppraise/?repage";
	}
	
	@RequiresPermissions("checks:atcAppraise:edit")
	@RequestMapping(value = "delete")
	public String delete(AtcAppraise atcAppraise, RedirectAttributes redirectAttributes) {
		atcAppraiseService.delete(atcAppraise);
		addMessage(redirectAttributes, "删除评分标准成功");
		return "redirect:"+Global.getAdminPath()+"/checks/atcAppraise/?repage";
	}

}