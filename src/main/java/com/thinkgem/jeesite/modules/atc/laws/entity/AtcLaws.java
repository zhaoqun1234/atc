 /****************************************************/
 /* Project Name:戒毒信息管理                             */
 /* File Name:AtcLaws.java              */
 /* Date:2017年2月21日下午5:59:12            */
 /* Description:法律手续信息的实体类     */
 /****************************************************/ 
package com.thinkgem.jeesite.modules.atc.laws.entity;

import java.util.Date;
import org.hibernate.validator.constraints.Length; 
import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 法律手续表Entity
 * @author 孟朋朋
 * @version 2017-03-15
 */
public class AtcLaws extends DataEntity<AtcLaws> {
	
	private static final long serialVersionUID = 1L;
	/* 公文头公*/
	private String gong;
	/* 戒毒人员姓名*/
	private String tableName;
	/* 公文头第*/
	private String di;
	/* 违法事件说明*/
	private String unlawfulAct;
	/* 违反法律名称*/
	private String lawName;
	/* 法律条目*/
	private String lawStrip;
	/* 法律款数*/
	private String lawSincere;
	/* 法律项数*/
	private String lawTerms;
	/* 备注信息*/
	private String remark;	
	/* 复议机关*/
	private String reconsiderDepartment;
	/* 戒毒所名称*/
	private String drugRehabilitationCenter;
	/* 戒毒所地址*/
	private String drugRehabilitationAddress;		



/**/
	/* 姓名         */
	private String name;
	/* 性别          */
	private String sex;
	/* 出生日期*/
	private Date birthdate;
	/* 证件类型*/
	private String identityType;
	/* 证件号码*/
	private String idcardNumber;
	/* 先住所*/
	private String nowPlace;
	/* 工作单位*/
	private String workUnit;
	/* 人员编号*/
	private String NO;
	/* 开始时间*/
	private Date startTime;
	/* 结束时间*/
	private Date endTime;


	

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}



	public AtcLaws() {
		super();
	}

	public AtcLaws(String id){
		super(id);
	}

	@Length(min=0, max=10, message="公文头公长度必须介于 0 和 10 之间")
	public String getGong() {
		return gong;
	}

	public void setGong(String gong) {
		this.gong = gong;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}


	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	public String getIdcardNumber() {
		return idcardNumber;
	}

	public void setIdcardNumber(String idcardNumber) {
		this.idcardNumber = idcardNumber;
	}

	public String getNowPlace() {
		return nowPlace;
	}

	public void setNowPlace(String nowPlace) {
		this.nowPlace = nowPlace;
	}

	public String getWorkUnit() {
		return workUnit;
	}

	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}

	public String getNO() {
		return NO;
	}

	public void setNO(String nO) {
		NO = nO;
	}

	@Length(min=0, max=64, message="戒毒人员姓名长度必须介于 0 和 64 之间")
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	@Length(min=0, max=20, message="公文头第长度必须介于 0 和 20 之间")
	public String getDi() {
		return di;
	}

	public void setDi(String di) {
		this.di = di;
	}
	
	@Length(min=0, max=200, message="违法事件说明长度必须介于 0 和 200 之间")
	public String getUnlawfulAct() {
		return unlawfulAct;
	}

	public void setUnlawfulAct(String unlawfulAct) {
		this.unlawfulAct = unlawfulAct;
	}
	
	@Length(min=0, max=30, message="违反法律名称长度必须介于 0 和 30 之间")
	public String getLawName() {
		return lawName;
	}

	public void setLawName(String lawName) {
		this.lawName = lawName;
	}
	
	@Length(min=0, max=20, message="法律条目长度必须介于 0 和 20 之间")
	public String getLawStrip() {
		return lawStrip;
	}

	public void setLawStrip(String lawStrip) {
		this.lawStrip = lawStrip;
	}
	
	@Length(min=0, max=20, message="法律款数长度必须介于 0 和 20 之间")
	public String getLawSincere() {
		return lawSincere;
	}

	public void setLawSincere(String lawSincere) {
		this.lawSincere = lawSincere;
	}
	
	@Length(min=0, max=20, message="法律项数长度必须介于 0 和 20 之间")
	public String getLawTerms() {
		return lawTerms;
	}

	public void setLawTerms(String lawTerms) {
		this.lawTerms = lawTerms;
	}
	
	@Length(min=0, max=200, message="备注信息长度必须介于 0 和 200 之间")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@Length(min=0, max=40, message="复议机关长度必须介于 0 和 40 之间")
	public String getReconsiderDepartment() {
		return reconsiderDepartment;
	}

	public void setReconsiderDepartment(String reconsiderDepartment) {
		this.reconsiderDepartment = reconsiderDepartment;
	}
	
	@Length(min=0, max=40, message="戒毒所名称长度必须介于 0 和 40 之间")
	public String getDrugRehabilitationCenter() {
		return drugRehabilitationCenter;
	}

	public void setDrugRehabilitationCenter(String drugRehabilitationCenter) {
		this.drugRehabilitationCenter = drugRehabilitationCenter;
	}
	
	@Length(min=0, max=40, message="戒毒所地址长度必须介于 0 和 40 之间")
	public String getDrugRehabilitationAddress() {
		return drugRehabilitationAddress;
	}

	public void setDrugRehabilitationAddress(String drugRehabilitationAddress) {
		this.drugRehabilitationAddress = drugRehabilitationAddress;
	}
	
}