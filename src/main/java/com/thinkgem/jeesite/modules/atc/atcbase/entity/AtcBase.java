 /****************************************************/
 /* Project Name:戒毒信息管理                             */
 /* File Name:AtBase.java              */
 /* Date:2017年2月21日下午5:59:12            */
 /* Description:基础信息的实体类     */
 /****************************************************/ 
package com.thinkgem.jeesite.modules.atc.atcbase.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 
 * @author Administrator
 *
 */
public class AtcBase extends DataEntity<AtcBase> {
	
	private static final long serialVersionUID = 1L;
	/*编号*/
	private String no;		
	/*姓名*/
	private String name;		
	/*别名*/
	private String alias;	
	/*性别*/
	private String sex;		
	/*出生日期*/
	private Date birthdate;		
	/*婚姻状态*/
	private String marital;	
	/*图像*/
	private String imagePeople;		
	/*民族*/
	private String nation;		
	/*文化程度*/
	private String culture;		
	/*身份证号码*/
	private String idcardNumber;	
	/*国籍*/
	private String country;		
	/*籍贯*/
	private String nativePlace;		
	/*职业*/
	private String job;		
	/*户口所在地*/
	private String address;		
	/*现居地*/
	private String nowPlace;	
	/*工作单位*/
	private String workUnit;		
	/*特殊体征*/
	private String specialSigns;		
	/*初次吸毒时间*/
	private Date fristDate;		
	/*以往吸毒次数*/
	private String times;		
	/*以往戒毒次数*/
	private String timesGiveupDrug;		
	/*吸食方式*/
	private String ways;		
	/*强制戒毒起始日期*/
	private Date starttime;		
	/*强制戒毒终止日期*/
	private Date endtime;		
	/*吸毒种类*/
	private String drugVarieties;		
	/*强制戒毒公安单位*/
	private String police;		
	/*公安机关联系电话*/
	private String telephone;		
	/*入所日期*/
	private Date inDate;		
	/*其他并行处罚*/
	private String punishMoney;		
	/*办案单位*/
	private String handleUntis;		
	/*病床号*/
	private String bedNumber;		
	/*医护人员*/
	private String medicalWork;		
	/*入所分类*/
	private String category;	
	/*人员分类*/
	private String personClassification;		
	/*管教民警*/
	private String policeDisciline;		
	/*协管民警*/
	private String assistPolice;		
	/*戒毒费*/
	private String drugFee;		
	/*零用钱*/
	private String pocketMoney;		
	/*何时何地经过机关处理*/
	private String officeDealWith;		
	/*戒毒人员个人简历*/
	private String drugPeopleIntroduce;		
	/*家庭人员及社会关系*/
	private String socialRelationship;		
	/*出所日期*/
	private Date outDate;		
	/*处所原因*/
	private String outReason;	
	/*去向*/
	private String goDirection;		
	/*填表人*/
	private String writeTablePeople;		
	
    public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}
    public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}
	private String identityType;
	private String parentId;
    private String parentIds;

	public AtcBase() {
		super();
	}

	public AtcBase(String id){
		super(id);
	}

	@Length(min=1, max=255, message="编号长度必须介于 1 和 255 之间")
	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}
	
	@Length(min=1, max=255, message="姓名长度必须介于 1 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=255, message="别名长度必须介于 0 和 255 之间")
	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	@Length(min=1, max=255, message="性别长度必须介于 1 和 255 之间")
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull(message="出生日期不能为空")
	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	
	@Length(min=1, max=255, message="婚姻状态长度必须介于 1 和 255 之间")
	public String getMarital() {
		return marital;
	}

	public void setMarital(String marital) {
		this.marital = marital;
	}
	
	@Length(min=1, max=1000, message="图像长度必须介于 1 和 1000 之间")
	public String getImagePeople() {
		return imagePeople;
	}

	public void setImagePeople(String imagePeople) {
		this.imagePeople = imagePeople;
	}
	
	@Length(min=1, max=255, message="民族长度必须介于 1 和 255 之间")
	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}
	
	@Length(min=0, max=255, message="文化程度长度必须介于 1 和 255 之间")
	public String getCulture() {
		return culture;
	}

	public void setCulture(String culture) {
		this.culture = culture;
	}
	
	@Length(min=1, max=255, message="身份证号码长度必须介于 1 和 18之间")
	public String getIdcardNumber() {
		return idcardNumber;
	}

	public void setIdcardNumber(String idcardNumber) {
		this.idcardNumber = idcardNumber;
	}
	
	@Length(min=1, max=255, message="国籍长度必须介于 1 和 255 之间")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	@Length(min=1, max=255, message="籍贯长度必须介于 1 和 255 之间")
	public String getNativePlace() {
		return nativePlace;
	}

	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}
	
	@Length(min=0, max=255, message="职业长度必须介于 0 和 255 之间")
	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}
	
	@Length(min=1, max=255, message="户口所在地长度必须介于 1 和 255 之间")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Length(min=1, max=255, message="现居地长度必须介于 1 和 255 之间")
	public String getNowPlace() {
		return nowPlace;
	}

	public void setNowPlace(String nowPlace) {
		this.nowPlace = nowPlace;
	}
	
	@Length(min=0, max=255, message="工作单位长度必须介于 0 和 255 之间")
	public String getWorkUnit() {
		return workUnit;
	}

	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}
	
	@Length(min=0, max=255, message="特殊体征长度必须介于 0 和 255 之间")
	public String getSpecialSigns() {
		return specialSigns;
	}

	public void setSpecialSigns(String specialSigns) {
		this.specialSigns = specialSigns;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull(message="初次吸毒时间不能为空")
	public Date getFristDate() {
		return fristDate;
	}

	public void setFristDate(Date fristDate) {
		this.fristDate = fristDate;
	}
	
	@Length(min=1, max=3, message="以往吸毒次数长度必须介于 1 和 3 之间")
	public String getTimes() {
		return times;
	}

	public void setTimes(String times) {
		this.times = times;
	}
	
	@Length(min=1, max=3, message="以往戒毒次数长度必须介于 1 和 3 之间")
	public String getTimesGiveupDrug() {
		return timesGiveupDrug;
	}

	public void setTimesGiveupDrug(String timesGiveupDrug) {
		this.timesGiveupDrug = timesGiveupDrug;
	}
	
	@Length(min=1, max=255, message="吸食方式长度必须介于 1 和 255 之间")
	public String getWays() {
		return ways;
	}

	public void setWays(String ways) {
		this.ways = ways;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull(message="强制戒毒起始日期不能为空")
	public Date getStarttime() {
		return starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull(message="强制戒毒终止日期不能为空")
	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	
	@Length(min=1, max=255, message="吸毒种类长度必须介于 1 和 255 之间")
	public String getDrugVarieties() {
		return drugVarieties;
	}

	public void setDrugVarieties(String drugVarieties) {
		this.drugVarieties = drugVarieties;
	}
	
	@Length(min=1, max=255, message="强制戒毒公安单位长度必须介于 1 和 255 之间")
	public String getPolice() {
		return police;
	}

	public void setPolice(String police) {
		this.police = police;
	}
	
	@Length(min=1, max=255, message="公安机关联系电话长度必须介于 1 和 255 之间")
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull(message="入所日期不能为空")
	public Date getInDate() {
		return inDate;
	}

	public void setInDate(Date inDate) {
		this.inDate = inDate;
	}
	
	@Length(min=1, max=255, message="其他并行处罚长度必须介于 1 和 255 之间")
	public String getPunishMoney() {
		return punishMoney;
	}

	public void setPunishMoney(String punishMoney) {
		this.punishMoney = punishMoney;
	}
	
	@Length(min=1, max=255, message="办案单位长度必须介于 1 和 255 之间")
	public String getHandleUntis() {
		return handleUntis;
	}

	public void setHandleUntis(String handleUntis) {
		this.handleUntis = handleUntis;
	}
	
	@Length(min=1, max=100, message="病床号长度必须介于 1 和 100 之间")
	public String getBedNumber() {
		return bedNumber;
	}

	public void setBedNumber(String bedNumber) {
		this.bedNumber = bedNumber;
	}
	
	@Length(min=1, max=255, message="医护人员长度必须介于 1 和 255 之间")
	public String getMedicalWork() {
		return medicalWork;
	}

	public void setMedicalWork(String medicalWork) {
		this.medicalWork = medicalWork;
	}
	
	@Length(min=1, max=255, message="入所分类长度必须介于 1 和 255 之间")
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	@Length(min=1, max=255, message="人员分类长度必须介于 1 和 255 之间")
	public String getPersonClassification() {
		return personClassification;
	}

	public void setPersonClassification(String personClassification) {
		this.personClassification = personClassification;
	}
	
	@Length(min=1, max=255, message="管教民警长度必须介于 1 和 255 之间")
	public String getPoliceDisciline() {
		return policeDisciline;
	}

	public void setPoliceDisciline(String policeDisciline) {
		this.policeDisciline = policeDisciline;
	}
	
	@Length(min=1, max=255, message="协管民警长度必须介于 1 和 255 之间")
	public String getAssistPolice() {
		return assistPolice;
	}

	public void setAssistPolice(String assistPolice) {
		this.assistPolice = assistPolice;
	}
	
	@Length(min=1, max=7, message="戒毒费长度必须介于 1 和 7 之间")
	public String getDrugFee() {
		return drugFee;
	}

	public void setDrugFee(String drugFee) {
		this.drugFee = drugFee;
	}
	
	@Length(min=1, max=7, message="零用钱长度必须介于 1 和 7 之间")
	public String getPocketMoney() {
		return pocketMoney;
	}

	public void setPocketMoney(String pocketMoney) {
		this.pocketMoney = pocketMoney;
	}
	
	@Length(min=1, max=2000, message="何时何地经过机关处理长度必须介于 1 和 2000 之间")
	public String getOfficeDealWith() {
		return officeDealWith;
	}

	public void setOfficeDealWith(String officeDealWith) {
		this.officeDealWith = officeDealWith;
	}
	
	@Length(min=1, max=2000, message="戒毒人员个人简历长度必须介于 1 和 2000 之间")
	public String getDrugPeopleIntroduce() {
		return drugPeopleIntroduce;
	}

	public void setDrugPeopleIntroduce(String drugPeopleIntroduce) {
		this.drugPeopleIntroduce = drugPeopleIntroduce;
	}
	
	@Length(min=1, max=10, message="家庭人员及社会关系长度必须介于 1 和 10 之间")
	public String getSocialRelationship() {
		return socialRelationship;
	}

	public void setSocialRelationship(String socialRelationship) {
		this.socialRelationship = socialRelationship;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull(message="出所日期不能为空")
	public Date getOutDate() {
		return outDate;
	}

	public void setOutDate(Date outDate) {
		this.outDate = outDate;
	}
	
	@Length(min=1, max=1000, message="处所原因长度必须介于 1 和 1000 之间")
	public String getOutReason() {
		return outReason;
	}

	public void setOutReason(String outReason) {
		this.outReason = outReason;
	}
	
	@Length(min=1, max=255, message="去向长度必须介于 1 和 255 之间")
	public String getGoDirection() {
		return goDirection;
	}

	public void setGoDirection(String goDirection) {
		this.goDirection = goDirection;
	}
	
	@Length(min=1, max=255, message="填表人长度必须介于 1 和 255 之间")
	public String getWriteTablePeople() {
		return writeTablePeople;
	}

	public void setWriteTablePeople(String writeTablePeople) {
		this.writeTablePeople = writeTablePeople;
	}
	
}