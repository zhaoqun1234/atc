/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.laws.service;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.atc.laws.entity.AtcLaws;

import com.thinkgem.jeesite.modules.atc.laws.dao.AtcLawsDao;

/**
 * 法律手续表Service
 * @author 孟朋朋
 * @version 2017-03-15
 */
@Service
@Transactional(readOnly = true)
public class AtcLawsService extends CrudService<AtcLawsDao, AtcLaws> {
	@Autowired
	private AtcLawsDao atcLawsDao;
	public AtcLaws get(String id) {
		return super.get(id);
	}
	
	public List<AtcLaws> findList(AtcLaws atcLaws) {
		return super.findList(atcLaws);
	}
	
	public Page<AtcLaws> findPage(Page<AtcLaws> page, AtcLaws atcLaws) {
		return super.findPage(page, atcLaws);
	}
	
	@Transactional(readOnly = false)
	public void save(AtcLaws atcLaws) {
		super.save(atcLaws);
	}
	
	@Transactional(readOnly = false)
	public void delete(AtcLaws atcLaws) {
		super.delete(atcLaws);
	}
	/**
	 * findRole:实现DAO里面的findRole方法
	 * @param tableName
	 * @return
	 */
	
	public List<AtcLaws> findRole(String tableName) {
		return atcLawsDao.findRole(tableName);
	}
}