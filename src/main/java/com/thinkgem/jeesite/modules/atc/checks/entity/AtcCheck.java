/**
 * Copyright &copy; 2012-2016 <a href="https:github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.checks.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 体检表Entity
 * @author 孙元智
 * @version 2017-03-15
 */
public class AtcCheck extends DataEntity<AtcCheck> {
	
	private static final long serialVersionUID = 1L;
	/*检查编号              */
	private String checkId;		 
	/*体重                  */
	private String checkWeight;		 
	/*身高                  */
	private String checkHeight;		 
	/*血型                  */
	private String checkBloodType;		 
	/*足长                  */
	private String checkLongEnough;		 
	/*有无残疾              */
	private String checkDisabled;		 
	/*曾患何种疾病          */
	private String checkDisease;		 
	/*曾做过何种手术或外伤史*/
	private String checkHistoryofsurgery;		 
	/*血压                 */
	private String checkBloodPressure;		 
	/*血压不正常备注       */
	private String checkBloodPressureRemark;		 
	/*肺部                 */
	private String checkLungs;		 
	/*心脏                 */
	private String checkHeart;		
	/*腹部                 */
	private String checkAbdomen;	 
	/*肺部不正常备注       */
	private String checkLungsRemark;		
	/*四肢关节             */
	private String checkLimbs;	 
	/*心脏不正常备注       */
	private String checkHeartRemark;		
	/*脊柱                 */
	private String checkSpine;		 
	/*胸透                 */
	private String checkThoracotomy;		
	/*腹部不正常备注       */
	private String checkAbdomenRemark;		 
	/*心电图               */
	private String checkElectrocardiogram;	
	/*B超                  */
	private String checkBsuper;		
	/*四肢关节不正常备注   */
	private String checkLimbsRemark;	 
	/*尿常规               */
	private String checkUrineRoutine;		 
	/*血常规               */
	private String checkBloodRoutine;		
	/*皮肤                 */
	private String checkSkin;		
	/*脊柱不正常备注       */
	private String checkSpineRemark;		
	/*体表损伤             */
	private String checkBodyDamage;		 
	/*肝功情况             */
	private String checkLiverFunction;		 
	/*胸透不正常备注       */
	private String checkThoracotomyRemark;		 
	/*性病情况             */
	private String checkSexuallyTransmittedDiseases;		
	/*心电图不正常备注     */
	private String checkElectrocardiogramRemark;		 
	/*艾滋病情况           */
	private String checkAidsSituation;		
	/*急性病情况           */
	private String checkAcuteDisease;		 
	/*B超不正常备注        */
	private String checkBsuperRemark;	 
	/*医生建议             */
	private String checkDoctorAdvises;	 
	/*领导建议             */
	private String checkLeadershipAdvice;		 
	/*尿常规不正常备注     */
	private String checkUrineRoutineRemark;		
	/*检查日期             */
	private Date checkDate;	 
	/*血常规不正常备注     */
	private String checkBloodRoutineRemark;		
	/*皮肤不正常备注       */
	private String checkSkinRemark;		
	/*体表损伤不正常备注   */
	private String checkBodyDamageRemark;		 
	/*肝功情况不正常备注   */
	private String checkLiverFunctionRemark;		
	/*性病备注             */
	private String checkSexuallyTransmittedDiseasesRemark;		 
	/*急性病情况备注       */
	private String checkAcuteDiseaseRemark;		
	/*人员编号             */
	private String no;          
	/*姓名                 */
	private String name;        
	
	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AtcCheck() {
		super();
	}

	public AtcCheck(String id){
		super(id);
	}

	@Length(min=0, max=64, message="检查编号长度必须介于 0 和 64 之间")
	public String getCheckId() {
		return checkId;
	}

	public void setCheckId(String checkId) {
		this.checkId = checkId;
	}
	
	@Length(min=0, max=64, message="体重长度必须介于 0 和 64 之间")
	public String getCheckWeight() {
		return checkWeight;
	}

	public void setCheckWeight(String checkWeight) {
		this.checkWeight = checkWeight;
	}
	
	@Length(min=0, max=64, message="身高长度必须介于 0 和 64 之间")
	public String getCheckHeight() {
		return checkHeight;
	}

	public void setCheckHeight(String checkHeight) {
		this.checkHeight = checkHeight;
	}
	
	@Length(min=0, max=64, message="血型长度必须介于 0 和 64 之间")
	public String getCheckBloodType() {
		return checkBloodType;
	}

	public void setCheckBloodType(String checkBloodType) {
		this.checkBloodType = checkBloodType;
	}
	
	@Length(min=0, max=64, message="足长长度必须介于 0 和 64 之间")
	public String getCheckLongEnough() {
		return checkLongEnough;
	}

	public void setCheckLongEnough(String checkLongEnough) {
		this.checkLongEnough = checkLongEnough;
	}
	
	@Length(min=0, max=64, message="有无残疾长度必须介于 0 和 64 之间")
	public String getCheckDisabled() {
		return checkDisabled;
	}

	public void setCheckDisabled(String checkDisabled) {
		this.checkDisabled = checkDisabled;
	}
	
	@Length(min=0, max=500, message="曾患何种疾病长度必须介于 0 和 500 之间")
	public String getCheckDisease() {
		return checkDisease;
	}

	public void setCheckDisease(String checkDisease) {
		this.checkDisease = checkDisease;
	}
	
	@Length(min=0, max=500, message="曾做过何种手术或外伤史长度必须介于 0 和 500 之间")
	public String getCheckHistoryofsurgery() {
		return checkHistoryofsurgery;
	}

	public void setCheckHistoryofsurgery(String checkHistoryofsurgery) {
		this.checkHistoryofsurgery = checkHistoryofsurgery;
	}
	
	@Length(min=0, max=64, message="血压长度必须介于 0 和 64 之间")
	public String getCheckBloodPressure() {
		return checkBloodPressure;
	}

	public void setCheckBloodPressure(String checkBloodPressure) {
		this.checkBloodPressure = checkBloodPressure;
	}
	
	@Length(min=0, max=500, message="血压不正常备注长度必须介于 0 和 500 之间")
	public String getCheckBloodPressureRemark() {
		return checkBloodPressureRemark;
	}

	public void setCheckBloodPressureRemark(String checkBloodPressureRemark) {
		this.checkBloodPressureRemark = checkBloodPressureRemark;
	}
	
	@Length(min=0, max=64, message="肺部长度必须介于 0 和 64 之间")
	public String getCheckLungs() {
		return checkLungs;
	}

	public void setCheckLungs(String checkLungs) {
		this.checkLungs = checkLungs;
	}
	
	@Length(min=0, max=64, message="心脏长度必须介于 0 和 64 之间")
	public String getCheckHeart() {
		return checkHeart;
	}

	public void setCheckHeart(String checkHeart) {
		this.checkHeart = checkHeart;
	}
	
	@Length(min=0, max=64, message="腹部长度必须介于 0 和 64 之间")
	public String getCheckAbdomen() {
		return checkAbdomen;
	}

	public void setCheckAbdomen(String checkAbdomen) {
		this.checkAbdomen = checkAbdomen;
	}
	
	@Length(min=0, max=500, message="肺部不正常备注长度必须介于 0 和 500 之间")
	public String getCheckLungsRemark() {
		return checkLungsRemark;
	}

	public void setCheckLungsRemark(String checkLungsRemark) {
		this.checkLungsRemark = checkLungsRemark;
	}
	
	@Length(min=0, max=64, message="四肢关节长度必须介于 0 和 64 之间")
	public String getCheckLimbs() {
		return checkLimbs;
	}

	public void setCheckLimbs(String checkLimbs) {
		this.checkLimbs = checkLimbs;
	}
	
	@Length(min=0, max=500, message="心脏不正常备注长度必须介于 0 和 500 之间")
	public String getCheckHeartRemark() {
		return checkHeartRemark;
	}

	public void setCheckHeartRemark(String checkHeartRemark) {
		this.checkHeartRemark = checkHeartRemark;
	}
	
	@Length(min=0, max=64, message="脊柱长度必须介于 0 和 64 之间")
	public String getCheckSpine() {
		return checkSpine;
	}

	public void setCheckSpine(String checkSpine) {
		this.checkSpine = checkSpine;
	}
	
	@Length(min=0, max=64, message="胸透长度必须介于 0 和 64 之间")
	public String getCheckThoracotomy() {
		return checkThoracotomy;
	}

	public void setCheckThoracotomy(String checkThoracotomy) {
		this.checkThoracotomy = checkThoracotomy;
	}
	
	@Length(min=0, max=500, message="腹部不正常备注长度必须介于 0 和 500 之间")
	public String getCheckAbdomenRemark() {
		return checkAbdomenRemark;
	}

	public void setCheckAbdomenRemark(String checkAbdomenRemark) {
		this.checkAbdomenRemark = checkAbdomenRemark;
	}
	
	@Length(min=0, max=64, message="心电图长度必须介于 0 和 64 之间")
	public String getCheckElectrocardiogram() {
		return checkElectrocardiogram;
	}

	public void setCheckElectrocardiogram(String checkElectrocardiogram) {
		this.checkElectrocardiogram = checkElectrocardiogram;
	}
	
	@Length(min=0, max=64, message="B超长度必须介于 0 和 64 之间")
	public String getCheckBsuper() {
		return checkBsuper;
	}

	public void setCheckBsuper(String checkBsuper) {
		this.checkBsuper = checkBsuper;
	}
	
	@Length(min=0, max=500, message="四肢关节不正常备注长度必须介于 0 和 500 之间")
	public String getCheckLimbsRemark() {
		return checkLimbsRemark;
	}

	public void setCheckLimbsRemark(String checkLimbsRemark) {
		this.checkLimbsRemark = checkLimbsRemark;
	}
	
	@Length(min=0, max=64, message="尿常规长度必须介于 0 和 64 之间")
	public String getCheckUrineRoutine() {
		return checkUrineRoutine;
	}

	public void setCheckUrineRoutine(String checkUrineRoutine) {
		this.checkUrineRoutine = checkUrineRoutine;
	}
	
	@Length(min=0, max=64, message="血常规长度必须介于 0 和 64 之间")
	public String getCheckBloodRoutine() {
		return checkBloodRoutine;
	}

	public void setCheckBloodRoutine(String checkBloodRoutine) {
		this.checkBloodRoutine = checkBloodRoutine;
	}
	
	@Length(min=0, max=64, message="皮肤长度必须介于 0 和 64 之间")
	public String getCheckSkin() {
		return checkSkin;
	}

	public void setCheckSkin(String checkSkin) {
		this.checkSkin = checkSkin;
	}
	
	@Length(min=0, max=500, message="脊柱不正常备注长度必须介于 0 和 500 之间")
	public String getCheckSpineRemark() {
		return checkSpineRemark;
	}

	public void setCheckSpineRemark(String checkSpineRemark) {
		this.checkSpineRemark = checkSpineRemark;
	}
	
	@Length(min=0, max=64, message="体表损伤长度必须介于 0 和 64 之间")
	public String getCheckBodyDamage() {
		return checkBodyDamage;
	}

	public void setCheckBodyDamage(String checkBodyDamage) {
		this.checkBodyDamage = checkBodyDamage;
	}
	
	@Length(min=0, max=64, message="肝功情况长度必须介于 0 和 64 之间")
	public String getCheckLiverFunction() {
		return checkLiverFunction;
	}

	public void setCheckLiverFunction(String checkLiverFunction) {
		this.checkLiverFunction = checkLiverFunction;
	}
	
	@Length(min=0, max=500, message="胸透不正常备注长度必须介于 0 和 500 之间")
	public String getCheckThoracotomyRemark() {
		return checkThoracotomyRemark;
	}

	public void setCheckThoracotomyRemark(String checkThoracotomyRemark) {
		this.checkThoracotomyRemark = checkThoracotomyRemark;
	}
	
	@Length(min=0, max=64, message="性病情况长度必须介于 0 和 64 之间")
	public String getCheckSexuallyTransmittedDiseases() {
		return checkSexuallyTransmittedDiseases;
	}

	public void setCheckSexuallyTransmittedDiseases(String checkSexuallyTransmittedDiseases) {
		this.checkSexuallyTransmittedDiseases = checkSexuallyTransmittedDiseases;
	}
	
	@Length(min=0, max=500, message="心电图不正常备注长度必须介于 0 和 500 之间")
	public String getCheckElectrocardiogramRemark() {
		return checkElectrocardiogramRemark;
	}

	public void setCheckElectrocardiogramRemark(String checkElectrocardiogramRemark) {
		this.checkElectrocardiogramRemark = checkElectrocardiogramRemark;
	}
	
	@Length(min=0, max=64, message="艾滋病情况长度必须介于 0 和 64 之间")
	public String getCheckAidsSituation() {
		return checkAidsSituation;
	}

	public void setCheckAidsSituation(String checkAidsSituation) {
		this.checkAidsSituation = checkAidsSituation;
	}
	
	@Length(min=0, max=64, message="急性病情况长度必须介于 0 和 64 之间")
	public String getCheckAcuteDisease() {
		return checkAcuteDisease;
	}

	public void setCheckAcuteDisease(String checkAcuteDisease) {
		this.checkAcuteDisease = checkAcuteDisease;
	}
	
	@Length(min=0, max=500, message="B超不正常备注长度必须介于 0 和 500 之间")
	public String getCheckBsuperRemark() {
		return checkBsuperRemark;
	}

	public void setCheckBsuperRemark(String checkBsuperRemark) {
		this.checkBsuperRemark = checkBsuperRemark;
	}
	
	@Length(min=0, max=500, message="医生建议长度必须介于 0 和 500 之间")
	public String getCheckDoctorAdvises() {
		return checkDoctorAdvises;
	}

	public void setCheckDoctorAdvises(String checkDoctorAdvises) {
		this.checkDoctorAdvises = checkDoctorAdvises;
	}
	
	@Length(min=0, max=500, message="领导建议长度必须介于 0 和 500 之间")
	public String getCheckLeadershipAdvice() {
		return checkLeadershipAdvice;
	}

	public void setCheckLeadershipAdvice(String checkLeadershipAdvice) {
		this.checkLeadershipAdvice = checkLeadershipAdvice;
	}
	
	@Length(min=0, max=500, message="尿常规不正常备注长度必须介于 0 和 500 之间")
	public String getCheckUrineRoutineRemark() {
		return checkUrineRoutineRemark;
	}

	public void setCheckUrineRoutineRemark(String checkUrineRoutineRemark) {
		this.checkUrineRoutineRemark = checkUrineRoutineRemark;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}
	
	@Length(min=0, max=500, message="血常规不正常备注长度必须介于 0 和 500 之间")
	public String getCheckBloodRoutineRemark() {
		return checkBloodRoutineRemark;
	}

	public void setCheckBloodRoutineRemark(String checkBloodRoutineRemark) {
		this.checkBloodRoutineRemark = checkBloodRoutineRemark;
	}
	
	@Length(min=0, max=500, message="皮肤不正常备注长度必须介于 0 和 500 之间")
	public String getCheckSkinRemark() {
		return checkSkinRemark;
	}

	public void setCheckSkinRemark(String checkSkinRemark) {
		this.checkSkinRemark = checkSkinRemark;
	}
	
	@Length(min=0, max=500, message="体表损伤不正常备注长度必须介于 0 和 500 之间")
	public String getCheckBodyDamageRemark() {
		return checkBodyDamageRemark;
	}

	public void setCheckBodyDamageRemark(String checkBodyDamageRemark) {
		this.checkBodyDamageRemark = checkBodyDamageRemark;
	}
	
	@Length(min=0, max=500, message="肝功情况不正常备注长度必须介于 0 和 500 之间")
	public String getCheckLiverFunctionRemark() {
		return checkLiverFunctionRemark;
	}

	public void setCheckLiverFunctionRemark(String checkLiverFunctionRemark) {
		this.checkLiverFunctionRemark = checkLiverFunctionRemark;
	}
	
	@Length(min=0, max=500, message="性病备注长度必须介于 0 和 500 之间")
	public String getCheckSexuallyTransmittedDiseasesRemark() {
		return checkSexuallyTransmittedDiseasesRemark;
	}

	public void setCheckSexuallyTransmittedDiseasesRemark(String checkSexuallyTransmittedDiseasesRemark) {
		this.checkSexuallyTransmittedDiseasesRemark = checkSexuallyTransmittedDiseasesRemark;
	}
	
	@Length(min=0, max=500, message="急性病情况备注长度必须介于 0 和 500 之间")
	public String getCheckAcuteDiseaseRemark() {
		return checkAcuteDiseaseRemark;
	}

	public void setCheckAcuteDiseaseRemark(String checkAcuteDiseaseRemark) {
		this.checkAcuteDiseaseRemark = checkAcuteDiseaseRemark;
	}
	
}