 /****************************************************/
 /* Project Name:戒毒信息管理                             */
 /* File Name:AtBaseController              */
 /* Date:2017年2月21日下午5:59:12            */
 /* Description:基础信息的业务逻辑层控制器        */
 /****************************************************/  
package com.thinkgem.jeesite.modules.atc.atcbase.web;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.atc.atcbase.entity.AtcBase;
import com.thinkgem.jeesite.modules.atc.atcbase.entity.AtcSocietyRelationship;
import com.thinkgem.jeesite.modules.atc.atcbase.service.AtcBaseService;
import com.thinkgem.jeesite.modules.atc.atcbase.service.AtcSocietyRelationshipService;

/**
 * 
 * @author zq
 *
 */
@Controller
@RequestMapping(value = "${adminPath}/atcbase/atcBase")
public class AtcBaseController extends BaseController {
	@Autowired
	private AtcBaseService atcBaseService;
	@Autowired
	private AtcSocietyRelationshipService atcSocietyRelationshipService;
	@ModelAttribute
	public AtcBase get(@RequestParam(required=false) String id) {
		AtcBase entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = atcBaseService.get(id);
		}
		if (entity == null){
			entity = new AtcBase();
		}
		return entity;
	}
	/**
	 * 
	 * list:(分类管理页面，获取所有的戒毒人员信息)
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param request:向服务器提交pagesize和pageNo的信息
	 * @param response:向客户端回应返回来的pagesize和pageno信息
	 * @param model:基础表的实体类entity
	 * @return 返回到分类查询页面
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = {"list", ""})
	public String list(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcBase> page = atcBaseService.findPage(new Page<AtcBase>(request, response), atcBase); 
	
		model.addAttribute("page", page);
		return "atc/atcbase/atcBaseList";
	}
	/**
	 * 
	 * form:(基础信息录入)
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param model:基础表的实体类entity
	 * @return 进入到基础信息录入页
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = "form")
	public String form(AtcBase atcBase, Model model) { 
		/*接口处*/
		 String  No=atcBaseService.findNo();
		atcBase.setNo(No);
		model.addAttribute("atcBase", atcBase);
		return "atc/atcbase/atcBaseForm";
	}
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = {"time"})
	public String time(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcBase> page = atcBaseService.findPage(new Page<AtcBase>(request, response), atcBase); 
		model.addAttribute("page", page);
		return "atc/atcbase/atcBaseListTime";
	}
	/**
	 * 
	 * seePrisoner:(查询犯人信息)
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param request:向服务器提交pagesize和pageNo的信息
	 * @param response:向客户端回应返回来的pagesize和pageno信息
	 * @param model:基础表的实体类entity
	 * @return 返回到戒毒人员信息查询页面
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = {"seePrisoner"})
	public String seePrisoner(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcBase> page = atcBaseService.findPage(new Page<AtcBase>(request, response), atcBase); 
		model.addAttribute("page", page);
		return "atc/atcbase/atcBaseListPrisoner";
	}
	/**
	 * 
	 * peopleSee:(人员信息查询)
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param request:向服务器提交pagesize和pageNo的信息
	 * @param response:向客户端回应返回来的pagesize和pageno信息
	 * @param model:基础表的实体类entity
	 * @return  返回到人员查询页面
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = {"peopleSee"})
	public String peopleSee(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcBase> page = atcBaseService.findPage(new Page<AtcBase>(request, response), atcBase); 
		model.addAttribute("page", page);
		return "atc/atcbase/atcBaseListPeople";
	}
	/**
	 * 
	 * lookMessage:(人员基础信息查看)
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param model:基础表的实体类entity
	 * @return 返回到人员基础信息查询页面
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = "lookMessage")
	public String lookMessage(AtcBase atcBase, Model model) { 
		List<AtcSocietyRelationship> relationship=atcSocietyRelationshipService.findRelationshipList(atcBase.getId());
		model.addAttribute("relationship",relationship);
		model.addAttribute("atcBase", atcBase);
		return "atc/atcbase/look_atcBase_message";
	}
	/**
	 * 
	 * atcBaseChange:(人员基础信息修改)
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param model:基础表的实体类entity
	 * @return  返回到基础信息修改页面
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = "atcBaseChange")
	public String atcBaseChange(AtcBase atcBase, Model model) { 
		List<AtcSocietyRelationship> relationship=atcSocietyRelationshipService.findRelationshipList(atcBase.getId());
		model.addAttribute("relationship",relationship);
		model.addAttribute("atcBase", atcBase);
		return "atc/atcbase/atcBaseChange";
	}
	/**
	 * 
	 * save:(保存录入的基础信息以及人员关系相关的信息)
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param model:基础表的实体类entity
	 * @param redirectAttributes:保存成功后的提示信息
	 * @param atcSocietyRelationship:社会人员关系实体类
	 * @param request:向服务器提交表单信息
	 * @return 返回到list方法
	 */
	@RequiresPermissions("atcbase:atcBase:edit")
	@RequestMapping(value = "save")
	public String save(AtcBase atcBase, Model model, RedirectAttributes redirectAttributes,AtcSocietyRelationship atcSocietyRelationship,
			HttpServletRequest request
			) {
		if (!beanValidator(model, atcBase)){
			return form(atcBase, model);
		}
		atcBaseService.save(atcBase);
		   String   actBaseId=atcBase.getId();
		 String[]   nameRelationship=request.getParameterValues("nameRelationship");
		 String[]   workUnit=request.getParameterValues("work_unit");
		 String[]   stayPlace=request.getParameterValues("stay_place");
		 String[]   telephoneNumber=request.getParameterValues("telephone_number");
		 String[]   relationshipPeople=request.getParameterValues("relationship_people");
		  for(int i=0;i<nameRelationship.length;i++){
			  if("".equals(nameRelationship[i])){
				  break;
			  }
			  AtcSocietyRelationship SocietyRelationship=new AtcSocietyRelationship();
			  SocietyRelationship.setActBaseId(actBaseId);
			  SocietyRelationship.setNameRelationship(nameRelationship[i]);
			  SocietyRelationship.setWorkUnit(workUnit[i]);
			  SocietyRelationship.setStayPlace(stayPlace[i]);
			  SocietyRelationship.setTelephoneNumber(telephoneNumber[i]);
			  SocietyRelationship.setRelationshipPeople(relationshipPeople[i]);
			  atcSocietyRelationshipService.save(SocietyRelationship);
		  }
		addMessage(redirectAttributes, "保存基本信息成功");
		return "redirect:"+Global.getAdminPath()+"/atcbase/atcBase/?repage";
	}
	/**
	 * 
	 * updateMessage:(更新基础表的信息基础)
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param model:基础表的实体类entity
	 * @param redirectAttributes:更新成功后的提示信息
	 * @param atcSocietyRelationship:社会人员关系实体类
	 * @param request:向服务器提交填写的表单信息
	 * @return  返回到list方法中
	 */
	@RequiresPermissions("atcbase:atcBase:edit")
	@RequestMapping(value = "updateMessage")
	public String updateMessage(AtcBase atcBase, Model model, RedirectAttributes redirectAttributes,AtcSocietyRelationship atcSocietyRelationship,
			HttpServletRequest request
			) {
		if (!beanValidator(model, atcBase)){
			return form(atcBase, model);
		}
		atcBaseService.save(atcBase);
		
		   String   actBaseId=atcBase.getId();
		 String[]   nameRelationship=request.getParameterValues("nameRelationship");
		 String[]   workUnit=request.getParameterValues("work_unit");
		 String[]   stayPlace=request.getParameterValues("stay_place");
		 String[]   telephoneNumber=request.getParameterValues("telephone_number");
		 String[]   relationshipPeople=request.getParameterValues("relationship_people");
		 String[]     id=request.getParameterValues("ID");
		 if(nameRelationship!=null){
				
			
		  for(int i=0;i<nameRelationship.length;i++){
			  AtcSocietyRelationship SocietyRelationship=new AtcSocietyRelationship();
			  SocietyRelationship.setId(id[i]);
			  SocietyRelationship.setActBaseId(actBaseId);
			  SocietyRelationship.setNameRelationship(nameRelationship[i]);
			  SocietyRelationship.setWorkUnit(workUnit[i]);
			  SocietyRelationship.setStayPlace(stayPlace[i]);
			  SocietyRelationship.setTelephoneNumber(telephoneNumber[i]);
			  SocietyRelationship.setRelationshipPeople(relationshipPeople[i]);
			  atcSocietyRelationshipService.updateMessage(SocietyRelationship);
		  }
		 }
		addMessage(redirectAttributes, "保存基本信息成功");
		return "redirect:"+Global.getAdminPath()+"/atcbase/atcBase/?repage";
	}
	/**
	 * 
	 * delete:(在人员分类查询页面中,根据id删除人员的基础信息)
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param redirectAttributes:删除成功后的提示信息
	 * @return 返回到list方法
	 */
	@RequiresPermissions("atcbase:atcBase:edit")
	@RequestMapping(value = "delete")
	public String delete(AtcBase atcBase, RedirectAttributes redirectAttributes) {
		atcBaseService.delete(atcBase);
		atcSocietyRelationshipService.delete_relationship(atcBase.getId());
		addMessage(redirectAttributes, "删除基本信息成功");
		return "redirect:"+Global.getAdminPath()+"/atcbase/atcBase/?repage";
	}
	/**
	 * 
	 * managerList:戒毒人员分类管理
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param request:向服务器提交pagesize和pageNo的信息
	 * @param response:向客户端回应返回来的pagesize和pageno信息
	 * @param model:基础表的实体类entity
	 * @return 返回到戒毒人员管理页面
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = {"managerList"})
	public String managerList(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcBase> page = atcBaseService.findPage(new Page<AtcBase>(request, response), atcBase); 
		model.addAttribute("page", page);
		return "atc/atcbase/atcBaseManagerList";
	}
	/**
	 * 
	 * drugCategory:(获取毒品类型管理信息)
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param request:向服务器提交pagesize和pageNo的信息
	 * @param response:向服务器提交pagesize和pageNo的信息
	 * @param model:基础表的实体类entity
	 * @return 返回到戒毒类型界面
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = {"drugCategory"})
	public String drugCategory(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcBase> page = atcBaseService.findPage(new Page<AtcBase>(request, response), atcBase); 
		model.addAttribute("page", page);
		return "atc/atcbase/drugCategory";
	}
	/**
	 * 
	 * schedule:(根据时间段查询获取戒毒人员信息)
	 * 
	 * @param atcBase:基础表的实体类对象
	 * @param request:向服务器提交pagesize和pageNo的信息
	 * @param response:向服务器提交pagesize和pageNo的信息
	 * @param model:基础表的实体类entity
	 * @return 返回到时间段查询页面
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = {"schedule"})
	public String schedule(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcBase> page = atcBaseService.findPageTimes(new Page<AtcBase>(request, response), atcBase); 
		model.addAttribute("page", page);
		return "atc/atcbase/scheduleList";
	}
	
	/*孙元智开始*/
	/**
	 * 
	 * outPersonBrowse:查看已出所的人员信息
	 * 
	 * @param atcBase:戒毒人员基本信息实体类对象
	 * @param model:将page和将要出所的人员信息返回到出所人员浏览页面，得到page和已出所的人员信息
	 * @return:执行方法后返回到出所人员浏览页面
	 */
	@RequestMapping(value = {"outPersonBrowse"})
	public String outPersonBrowse(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcBase> page = atcBaseService.findPageLists(new Page<AtcBase> (request, response), atcBase); 
		model.addAttribute("page", page);
			return "atc/atcbase/atcBaseOutPersonList";
	}
	/**
	 * 
	 * outPersonBrowse:未来十天将要出所的人员
	 * 
	 * @param atcBase:戒毒人员基本信息实体类对象
	 * @param model:将page和将要出所的人员信息返回到未来十天将要出所人员jsp页面，得到page和将要出所的人员信息
	 * @return:执行方法后返回到未来十天将要出所人员页面
	 */
	@RequestMapping(value = {"outPerson"})
	public String outPerson(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcBase> page = atcBaseService.findPages(new Page<AtcBase> (request, response), atcBase); 
		model.addAttribute("page", page);
			return "atc/atcbase/outTenDaysPersonList";
	}
	
	/**
	 * 
	 * outPerson:未来二十天将要出所的人员
	 * 
	 * @param atcBase:戒毒人员基本信息实体类对象
	 * @param model:将page和将要出所的人员信息返回到未来二十天将要出所人员jsp页面，得到page和将要出所的人员信息
	 * @return:执行方法后返回到未来二十天将要出所人员页面
	 */
	@RequestMapping(value = {"outTwentyDaysPerson"})
	public String outTwentyDaysPerson(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcBase> page = atcBaseService.findTwentyPage(new Page<AtcBase> (request, response), atcBase); 
		model.addAttribute("page", page);
			return "atc/atcbase/outTwentyDaysPersonList";
	}
	
	/**
	 * 
	 * outThirtyDaysPerson:未来三十天将要出所的人员
	 * 
	 * @param atcBase:戒毒人员基本信息实体类对象
	 * @param model:将page和将要出所的人员信息返回到未来三十天将要出所人员jsp页面，得到page和将要出所的人员信息
	 * @return:执行方法后返回到未来三十天将要出所人员页面
	 */
	@RequestMapping(value = {"outThirtyDaysPerson"})
	public String outThirtyDaysPerson(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcBase> page = atcBaseService.findThirtyPage(new Page<AtcBase> (request, response), atcBase); 
		model.addAttribute("page", page);
			return "atc/atcbase/outThirtyDaysPersonList";
	}
	
	/*孙元智结束*/
	
	/****************************************曹红***************************************************
	 *戒毒人员基本信息打印预览 
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = "printView")
	public String printView(AtcBase atcBase, Model model) { 
		model.addAttribute("atcBase", atcBase);
		return "atc/atcbase/atcBasePrintView";
	}
	/*系统自动生成的方法*/
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(@RequestParam(required=false) String extId, HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<AtcBase> list = atcBaseService.findList(new AtcBase());
		for (int i=0; i<list.size(); i++){
			AtcBase e = list.get(i);
			if (StringUtils.isBlank(extId) || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParentId());
				map.put("name", e.getName());
				mapList.add(map);
			}
		}
		return mapList;
	}
	/**
	 * 
	 * printer:(生成world文旦的相关方法)
	 * 
	 * @param atcBase:戒毒人员基本信息实体类对象
	 * @param request:向服务器提交pagesize和pageNo的信息
	 * @param response: 向客户端回应pagesize和pageno的相关信息
	 * @param model:基础表的实体类entity
	 * @param atcSocietyRelationship:人员社会关系实体类enity
	 * @param redirectAttributes:生成word文档成功后的提示信息
	 * @return 返回到list方法中
	 */
	
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = {"printer"})
	public String printer(AtcBase atcBase, HttpServletRequest request, HttpServletResponse response, Model model){
			 
		AtcBase page = atcBaseService.get(atcBase.getId()); 
		
		List<AtcSocietyRelationship> relationship=atcSocietyRelationshipService.findRelationshipList(atcBase.getId());
		
		
		model.addAttribute("page", page);
		model.addAttribute("relationship", relationship);
		
		
		 return "atc/atcbase/atcBasePrintView";
	}
	/*杨雪超controller开始*/
	/**
	 * 
	 * sex:这里是对返回的man和woman人数的获取人数并且进行转参
	 * 
	 * @param atcBase 人员信息表的实体类
	 * @param model 把数据从controller传递到前端的载体
	 * @return 返回到根据性别统计的页面,并且把参数传递到前端的
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = "sex")
	public String sex(AtcBase atcBase, Model model) { 
		List<Map<String, Object>> key=null;
		key=atcBaseService.sex();
		model.addAttribute("key", key);
		model.addAttribute("atcBase", atcBase);
		return "atc/count/sex";
	}
	
	/**
	 * 
	 * education:这里是对获取不同教育程度人数数据进行赋值并且传递参数
	 * 
	 * @param atcBase 人员基本信息的实体类
	 * @param model 把数据从controller传递到前端的载体
	 * @return 返回到根据学历统计的页面,并且把数据传递到页面上
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = "education")
	public String education(AtcBase atcBase, Model model) { 
		List<Map<String, Object>> key =null;
		key = atcBaseService.education();
		model.addAttribute("key", key);
		model.addAttribute("atcBase", atcBase);
		return "atc/count/education";
	}
	/**
	 * 
	 * national:此处是根据获取前三个最多人数的民族并且排序排序,返回的是一个List的键值对的集合
	 * 
	 * @param atcBase 人员基本信息的实体类
	 * @param model 把数据从后端传递到前端的载体
	 * @return 返回到根据不同民族统计人数的饼状图的页面,并且带过去参数
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = "national")
	public String national(AtcBase atcBase, Model model) { 
		List<Map<String, Object>> key=null;
		/*这里是求女性的人数*/
		key = atcBaseService.national();
		/*这里是男性的人数*/
		model.addAttribute("key", key);
		model.addAttribute("atcBase", atcBase);
		return "atc/count/national";
	}
	/**
	 * 
	 * age:这里是根据不同年龄段的人数
	 * 
	 * @param atcBase 人员基本信息的实体类
	 * @param model 把数据从后端传递到前端的一个载体
	 * @return 返回到根据不同年龄段的人员统计页面,并且把参数带过去.
	 */
	@RequiresPermissions("atcbase:atcBase:view")
	@RequestMapping(value = "age")
	public String age(AtcBase atcBase, Model model) { 
		double underTwenty=0;
		double twentyFive=0;
		double thirty=0;
		double thirtyFive=0;
		double moreThirty=0;
		/*这里是求高中以下的人数*/
		underTwenty = atcBaseService.underTwenty();
		System.out.println(underTwenty);
		/*这里是高中的人数*/
		twentyFive = atcBaseService.twentyFive();
		/*这里是大专的学历*/
		thirty = atcBaseService.thirty();
		/*这里是本科及以上的学历*/
		thirtyFive = atcBaseService.thirtyFive();
		/*这里是35岁以上的*/
		moreThirty = atcBaseService.moreThirty();
		model.addAttribute("underTwenty", underTwenty);
		model.addAttribute("twentyFive", twentyFive);
		model.addAttribute("thirty", thirty);
		model.addAttribute("thirtyFive", thirtyFive);
		model.addAttribute("moreThirty", moreThirty);
		model.addAttribute("atcBase", atcBase);
		return "atc/count/age";
	}

	/*杨雪超controller结束*/
}