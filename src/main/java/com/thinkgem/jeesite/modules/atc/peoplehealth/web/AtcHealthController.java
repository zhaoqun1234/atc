/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.peoplehealth.web;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.atc.peoplehealth.entity.AtcHealth;
import com.thinkgem.jeesite.modules.atc.peoplehealth.service.AtcHealthService;

/**
 * 人员健康检查表Controller(控制层)
 * @author 曹红
 * @version 2017-03-13
 */
@Controller
@RequestMapping(value = "${adminPath}/peoplehealth/atcHealth")
public class AtcHealthController extends BaseController {
	
	/*人员健康检查表Service(服务层)对象*/
	@Autowired
	private AtcHealthService atcHealthService;
	
	/**
	 * --自动为此文件内的每个方法里的AtcHealth参数赋值。
	 * 根据戒毒人员的id获得人员健康检查表对应的实体类对象
	 * @param id 戒毒人员的编号
	 * @return 人员健康检查表对应的实体类对象
	 */
	@ModelAttribute
	public AtcHealth get(@RequestParam(required=false) String id) {
		AtcHealth entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = atcHealthService.get(id);
		}
		if (entity == null){
			entity = new AtcHealth();
		}
		return entity;
	}
	
	/**
	 * 戒毒人员健康表打印预览
	 * @param atcHealth 要打印的人员
	 * @param model 传递参数的对象 传递参数的对象
	 * @return 返回到 戒毒人员健康表打印预览页面
	 */
	@RequiresPermissions("peoplehealth:atcHealth:view")
	@RequestMapping(value = "printView")
	public String printView(AtcHealth atcHealth, Model model) {
		model.addAttribute("atcHealth", atcHealth);
		return "atc/peoplehealth/atcHealthPrintView";
	}
	
	/**
	 * 简单展示所有戒毒人员信息
	 * @param atcHealth 
	 * @param request 页面的请求信息
	 * @param response 响应到页面的信息
	 * @param model 传递参数的对象
	 * @return 戒毒人员信息展示页面
	 */
	@RequiresPermissions("peoplehealth:atcHealth:view")
	@RequestMapping(value = {"list", ""})
	public String list(AtcHealth atcHealth, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<AtcHealth> page = atcHealthService.findPage(new Page<AtcHealth>(request, response), atcHealth); 
		model.addAttribute("page", page);
		return "atc/peoplehealth/atcHealthList";
	}

	/**
	 * 1.通过该方法跳转到 添加戒毒人员信息 的详情显示页面
	 * @param atcHealth 要查看详情的 戒毒人员
	 * @param model 传递参数的对象
	 * @return  戒毒人员信息 查看页面
	 */
	@RequiresPermissions("peoplehealth:atcHealth:view")
	@RequestMapping(value = "view")
	public String view(AtcHealth atcHealth, Model model) {
		model.addAttribute("atcHealth", atcHealth);
		return "atc/peoplehealth/atcHealthView";
	}
	/**
	 * 1.通过该方法跳转到 添加戒毒人员信息 的页面
	 * 2.通过此方法携带要修改的戒毒人员的信息，跳转到 戒毒人员信息 修改页面
	 * @param atcHealth 要修改的 戒毒人员
	 * @param model 传递参数的对象
	 * @return  添加戒毒人员信息 的页面  或  戒毒人员信息 修改页面
	 */
	@RequiresPermissions("peoplehealth:atcHealth:view")
	@RequestMapping(value = "form")
	public String form(AtcHealth atcHealth, Model model) {
		model.addAttribute("atcHealth", atcHealth);
		return "atc/peoplehealth/atcHealthForm";
	}

	/**
	 * 保存戒毒人员的信息
	 * @param atcHealth 要保存的戒毒人员
	 * @param model 传递参数的对象
	 * @param redirectAttributes 转发到其它页面的 转发器
	 * @return 戒毒人员信息展示页面
	 */
	@RequiresPermissions("peoplehealth:atcHealth:edit")
	@RequestMapping(value = "save")
	public String save(AtcHealth atcHealth, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, atcHealth)){
			return form(atcHealth, model);
		}
		atcHealthService.save(atcHealth);
		addMessage(redirectAttributes, "保存保存人员健康信息成功成功");
		return "redirect:"+Global.getAdminPath()+"/peoplehealth/atcHealth/?repage";
	}
	
	/**
	 * 删除指定戒毒人员的信息
	 * @param atcHealth 要删除信息的戒毒人员
	 * @param redirectAttributes 转发到其它页面的 转发器
	 * @return 戒毒人员信息展示页面
	 */
	@RequiresPermissions("peoplehealth:atcHealth:edit")
	@RequestMapping(value = "delete")
	public String delete(AtcHealth atcHealth, RedirectAttributes redirectAttributes) {
		atcHealthService.delete(atcHealth);
		addMessage(redirectAttributes, "删除保存人员健康信息成功成功");
		return "redirect:"+Global.getAdminPath()+"/peoplehealth/atcHealth/?repage";
	}
	
	/**
	 * ajax验证
	 * 根据人员健康检查表的外键查询相关人员的人数,
	 * 以此来判断该人员的健康检查信息是否录入
	 * @param fkId 人员健康检查表的外键
	 * @param response
	 * @param redirectAttributes
	 */
	@RequiresPermissions("peoplehealth:atcHealth:view")
	@RequestMapping(value = "findRole")
	public void findRole(HttpServletResponse response ,String fkId,RedirectAttributes redirectAttributes){
		int atcHealthNums =atcHealthService.findRole(fkId);
		response.setContentType("application/text;charset=utf-8");
		try {
			if(atcHealthNums > 0){
				response.getWriter().print("true");
			}else{
				response.getWriter().print("false");
			}
		} catch (IOException e) {
			addMessage(redirectAttributes, "用户校验失败！失败信息：" + e.getMessage());
		}
	}
	
	/**
	 * 
	 * printer:(按一定模板，打印法律手续)
	 * 
	 * @param atcLaw
	 * @param model
	 * @return
	 */
	/*@RequiresPermissions("peoplehealth:atcHealth:view")
	@RequestMapping(value = "printer")
	public String printer(AtcHealth atcHealth, Model model,AtcBase atcBase,RedirectAttributes redirectAttributes) {
		model.addAttribute("AtcHealth", atcHealth);
		Word world=new Word();
		world.createDocAtcHealth(atcHealth);
		addMessage(redirectAttributes,"恭喜你,word文档以生成在D:/文件下");
		return "redirect:"+Global.getAdminPath()+"/peoplehealth/atcHealth/list/?repage";
	}*/
	
	@RequiresPermissions("peoplehealth:atcHealth:view")
	@RequestMapping(value = "printer")
	public String printer(AtcHealth atcHealth, Model model) {
		model.addAttribute("atcHealth", atcHealth);
		return "atc/peoplehealth/atcHealthPrintView";
	}
	
	/**
	 * 
	 * printer:(按一定模板，打印法律手续)
	 * 
	 * @param atcLaw
	 * @param model
	 * @return
	 */
	@RequiresPermissions("peoplehealth:atcHealth:view")
	@RequestMapping(value = "LawView")
	public String LawView( Model model) {
		return "atc/law/atcLawPrintView";
	}
	

}