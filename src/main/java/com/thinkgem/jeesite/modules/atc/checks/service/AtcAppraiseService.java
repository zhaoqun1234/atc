/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.checks.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.atc.checks.entity.AtcAppraise;
import com.thinkgem.jeesite.modules.atc.checks.dao.AtcAppraiseDao;

/**
 * 评分标准表Service
 * @author 孙元智
 * @version 2017-03-13
 */
@Service
@Transactional(readOnly = true)
public class AtcAppraiseService extends CrudService<AtcAppraiseDao, AtcAppraise> {

	public AtcAppraise get(String id) {
		return super.get(id);
	}
	
	public List<AtcAppraise> findList(AtcAppraise atcAppraise) {
		return super.findList(atcAppraise);
	}
	
	public Page<AtcAppraise> findPage(Page<AtcAppraise> page, AtcAppraise atcAppraise) {
		return super.findPage(page, atcAppraise);
	}
	
	@Transactional(readOnly = false)
	public void save(AtcAppraise atcAppraise) {
		super.save(atcAppraise);
	}
	
	@Transactional(readOnly = false)
	public void delete(AtcAppraise atcAppraise) {
		super.delete(atcAppraise);
	}
	
}