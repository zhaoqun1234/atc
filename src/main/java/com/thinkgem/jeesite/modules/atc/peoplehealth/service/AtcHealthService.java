/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.peoplehealth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.atc.peoplehealth.dao.AtcHealthDao;
import com.thinkgem.jeesite.modules.atc.peoplehealth.entity.AtcHealth;

/**
 * 人员健康检查表Service
 * @author 曹红
 * @version 2017-03-13
 */
@Service
@Transactional(readOnly = true)
public class AtcHealthService extends CrudService<AtcHealthDao, AtcHealth> {
	@Autowired
	private AtcHealthDao atcHealthDao;
	public AtcHealth get(String id) {
		return super.get(id);
	}
	
	public List<AtcHealth> findList(AtcHealth atcHealth) {
		return super.findList(atcHealth);
	}
	
	public Page<AtcHealth> findPage(Page<AtcHealth> page, AtcHealth atcHealth) {
		return super.findPage(page, atcHealth);
	}
	
	@Transactional(readOnly = false)
	public void save(AtcHealth atcHealth) {
		super.save(atcHealth);
	}
	
	@Transactional(readOnly = false)
	public void delete(AtcHealth atcHealth) {
		super.delete(atcHealth);
	}
	
	/**
	 * 根据人员健康检查表的外键查询相关人员的人数,
	 * 以此来判断该人员的健康检查信息是否录入
	 * @param fkId 人员健康检查表的外键
	 * @return 查询到的人数   ==0：该人员没有录入，>0,该人员已经录入
	 */
	public int findRole(String fkId ){
		return atcHealthDao.findRole(fkId);
	}
	
}