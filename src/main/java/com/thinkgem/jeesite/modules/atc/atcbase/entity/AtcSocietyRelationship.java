 /****************************************************/
 /* Project Name:戒毒信息管理                             */
 /* File Name:AtcSocietyRelationship              */
 /* Date:2017年2月21日下午5:59:12            */
 /* Description:   社会人员关系实体类    */
 /****************************************************/  
package com.thinkgem.jeesite.modules.atc.atcbase.entity;

import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 
 * @author Administrator
 *
 */
public class AtcSocietyRelationship extends DataEntity<AtcSocietyRelationship> {
	
	private static final long serialVersionUID = 1L;
	/*人员基础信息表对应的id*/
	private String actBaseId;		
	/*关系人姓名*/
	private String nameRelationship;		
	/*工作单位*/
	private String workUnit;		
	/*住所*/
	private String stayPlace;		
	/*联系电话*/
	private String telephoneNumber;		
	/*人员关系*/
	private String relationshipPeople;		
	
	public AtcSocietyRelationship() {
		super();
	}

	public AtcSocietyRelationship(String id){
		super(id);
	}

	@Length(min=1, max=255, message="人员基础信息表对应的id长度必须介于 1 和 255 之间")
	public String getActBaseId() {
		return actBaseId;
	}

	public void setActBaseId(String actBaseId) {
		this.actBaseId = actBaseId;
	}
	
	@Length(min=1, max=25, message="姓名长度必须介于 1 和 25 之间")
	public String getNameRelationship() {
		return nameRelationship;
	}

	public void setNameRelationship(String nameRelationship) {
		this.nameRelationship = nameRelationship;
	}
	
	@Length(min=1, max=25, message="工作单位长度必须介于 1 和 25 之间")
	public String getWorkUnit() {
		return workUnit;
	}

	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}
	
	@Length(min=1, max=255, message="住所长度必须介于 1 和 255 之间")
	public String getStayPlace() {
		return stayPlace;
	}

	public void setStayPlace(String stayPlace) {
		this.stayPlace = stayPlace;
	}
	
	@Length(min=1, max=255, message="联系电话长度必须介于 1 和 255 之间")
	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	
	@Length(min=1, max=255, message="人员关系长度必须介于 1 和 255 之间")
	public String getRelationshipPeople() {
		return relationshipPeople;
	}

	public void setRelationshipPeople(String relationshipPeople) {
		this.relationshipPeople = relationshipPeople;
	}
	
}