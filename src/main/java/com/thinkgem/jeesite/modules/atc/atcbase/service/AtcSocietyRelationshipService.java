/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.atc.atcbase.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.atc.atcbase.entity.AtcSocietyRelationship;
import com.thinkgem.jeesite.modules.atc.atcbase.dao.AtcSocietyRelationshipDao;

/**
 * 基本信息Service
 * @author zq
 * @version 2017-03-08
 */
@Service
@Transactional(readOnly = true)
public class AtcSocietyRelationshipService extends CrudService<AtcSocietyRelationshipDao, AtcSocietyRelationship> {
	@Autowired
	private AtcSocietyRelationshipDao atcSocietyRelationshipDao;
	
	public AtcSocietyRelationship get(String id) {
		return super.get(id);
	}
	
	public List<AtcSocietyRelationship> findList(AtcSocietyRelationship atcSocietyRelationship) {
		return super.findList(atcSocietyRelationship);
	}
	
	public Page<AtcSocietyRelationship> findPage(Page<AtcSocietyRelationship> page, AtcSocietyRelationship atcSocietyRelationship) {
		return super.findPage(page, atcSocietyRelationship);
	}
	
	@Transactional(readOnly = false)
	public void save(AtcSocietyRelationship atcSocietyRelationship) {
		super.save(atcSocietyRelationship);
	}
	
	@Transactional(readOnly = false)
	public void delete(AtcSocietyRelationship atcSocietyRelationship) {
		super.delete(atcSocietyRelationship);
	}

	public List<AtcSocietyRelationship> findRelationshipList(String id) {
		return atcSocietyRelationshipDao.findRelationshipList(id);
	}
	@Transactional(readOnly = false)
	public int delete_relationship(String id) {
		return atcSocietyRelationshipDao.delete_relationship(id);
		
	}
	@Transactional(readOnly = false)
	public int updateMessage(AtcSocietyRelationship societyRelationship) {
		return atcSocietyRelationshipDao.updateMessage(societyRelationship);
	}
	
}